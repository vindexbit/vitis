Basic format for command 'mkfs':
    vitis mkfs -i <filesystem.image> [-s <размер>]

This command creates a filesystem image for mounting.

The "-i" parameter specifies the path to the file serving as the FS image.
The optional '-s' parameter points the image size in bytes or other units
when using special letter suffix after the number:
    B — specifies the size in bytes;
    K — specifies the size in b kibibytes;
    M — specifies the size in b mebibytes;
    G — specifies the size in b gibibytes;
    T — specifies the size in b tebibytes.
If the size is not specified, the default value of 10G is used.
If the size is provided without a letter, it's interpreted as the bytes number.

Example:
    vitis mkfs -i vitis.fs -s 2T
    A file 'vitis.fs' with a size of two tebibytes will be created
    and formatted as an ext4 filesystem.

Additional details

The base filesystem for Vitis is ext4, meaning the created image
is essentially an ext4 partition image with specially organized data
used by Vitis.

Vitis uses loop devices. This allows a file to be used
as a block device. For more details about loop devices,
you can refer to the manual:
    man 4 loop
    man 8 losetup
