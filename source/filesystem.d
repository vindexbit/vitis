/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2021-2025
 *
 * Distributed under
 * the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module filesystem;

/* CORE */

import unix_dirent = core.sys.posix.dirent;
import unix_fcntl = core.sys.posix.fcntl;
import core.sys.posix.fcntl :
    O_RDONLY, O_WRONLY, O_RDWR, O_ACCMODE, O_TRUNC, O_APPEND, O_NOFOLLOW;
import unix_stat = core.sys.posix.sys.stat;
import core.sys.posix.sys.stat :
    S_IWUSR, S_IWGRP, S_IWOTH,
    S_IFSOCK, S_IFLNK, S_IFREG, S_IFBLK, S_IFDIR, S_IFCHR, S_IFIFO,
    utimensat;

import unix_statvfs = core.sys.posix.sys.statvfs;
import unix_unistd = core.sys.posix.unistd;
import core.sys.posix.unistd : F_OK, R_OK, W_OK, X_OK;
import unix_ioctl = core.sys.posix.sys.ioctl;

import core.atomic;
import core.stdc.string : memset, memcpy;
import core.stdc.stdio : fread;
import core.stdc.errno;
import core.sync.mutex : Mutex;
import core.sys.linux.sys.xattr : XATTR_CREATE, XATTR_REPLACE;
import core.sys.posix.time : timespec, time_t;


/* STD */

import std.algorithm :
    any, canFind, each, endsWith, filter,
    map, predSwitch, remove, sort, startsWith, reverse;
import std.array : array, join, split, replace;
import std.bitmanip : BitArray, swapEndian;
import std.conv : oct = octal, to, parse;
static import std.csv;
import std.exception : enforce, collectException;
static import std.file;
import std.file : readText;
import std.format : format;
import std.functional : memoize;
import std.getopt : getopt;
import std.meta : S = AliasSeq;
import std.path : baseName, buildPath, dirName, extension, pathSplitter;
import std.process : thisThreadID;
import std.range : empty, takeOne, repeat, take, walkLength;
import std.string : fromStringz, indexOf, splitLines, toStringz;
import std.stdio : File, write, writeln, stderr;
import std.typecons : Tuple, tuple, Flag, Yes, No;
import std.uni : toLower;
import std.variant : Variant;
static import std.digest.crc;


/* AMALTHEA */

import amalthea.csv : CSV, CsvException;
static import amalthea.fileformats;

static import amalthea.fs;
import amalthea.fs :
    exists, FileEntry, getAbsolutePath, getFileTypeFromFileMode, isDir,
    getDirectories, FileType, getFiles, getRegularFiles, isRegularFile,
    isSymlink, xattr, DirReader, applyFileAdvise, FileAdvice;

import amalthea.dataprocessing :
    isAmong, getIndex, stripLeft, stripRight,
    calcUnion, calcComplement, calcIntersection,
    countElements, divideByElemNumber;


/* OXFUSE */

import oxfuse;


/* LOCAL MODULES */

static import rpn;
import rpn : ET;


/******************************************************************************/

// Pseudocategories.
immutable expEnding = "\\x";
immutable attrPCPrefix = "@";
immutable fileNameListEnding = "\\names";
immutable filePathListEnding = "\\paths";

// Pseudofiles.
immutable noteEnding = "\\note";
immutable imLinkEnding = "\\l";

immutable repeatedNameEnding = "\\r";

enum ino_t defaultInoZoneLength = 2UL ^^ 40UL;
enum ino_t offsetAttrPC         = 0x10000000000UL;
enum ino_t offsetNameLists      = 0x20000000000UL;
enum ino_t offsetNotes          = 0x30000000000UL;
enum ino_t offsetExpressions    = 0x40000000000UL;
enum ino_t offsetImaginaryLinks = 0x50000000000UL;
enum ino_t offsetPathList       = 0x60000000000UL;
enum ino_t offsetPathItem       = 0x70000000000UL;

enum ELOOP_LIMIT = 40;
enum LIMIT_NLINKS = 4096;

enum ino_t rootIno = 1UL;

enum mode_t dirFullAccess = oct!777;
enum mode_t othFullAccess = oct!666;
enum mode_t antitypeMask = oct!7777;

/// Manipulates file space. Linux-specific system call.
extern(C) int fallocate(int, int, off_t, off_t) nothrow @nogc;
alias linux_fallocate = fallocate;

/// Apply or remove an advisory lock on an open file.
extern(C) int flock(int fd, int operation) nothrow @nogc;
alias linux_flock = flock;

/// Set file mode creation mask.
extern(C) mode_t umask(mode_t mask);

mode_t getCurrentUmask() {
    mode_t mask = umask(0);
    umask(mask);
    return mask;
}

enum RENAME_NOREPLACE = 1;
enum RENAME_EXCHANGE = 2;
enum RENAME_WHITEOUT = 4;

enum UTIME_OMIT = 0x3ffffffe;
enum UTIME_NOW = 0x3fffffff;
extern(C) private int clock_gettime(int __clock_id, timespec *__tp);


immutable openFlagsForWriting = [O_WRONLY, O_RDWR, O_APPEND, O_TRUNC];


/// File access mask with write prohibition for all users.
enum noWriteMask = ~S_IWUSR & ~S_IWGRP & ~S_IWOTH;


/// Safe screen printing without exceptions.
void safeWriteln(S...)(S args) nothrow {
    try stderr.writeln(args);
    catch (Exception e) { }
}
/// ditto
void safeWritefln(S...)(S args) nothrow {
    try stderr.writefln(args);
    catch (Exception e) { }
}


// Error messages.
immutable manyOpenedDirsMsg = "Too many open directories.";


/// Main class with file operations of Vindex's Tag file System.
class Vitis : FileSystem {

private:

    static immutable char[] unacceptableSymbols = ['\n', '\r', '\\'];

    string fsFile;
    string pseudoStorage;
    string inodesStorage;

    OpenInodeTable inodeTable;
    InoGenerator inoGenerator;

    ino_t[ulong] repeatedNameCases;  // keys are TIDs, values are inode numbers
    ino_t[ulong] imaginaryLinkCases;  // keys are TIDs, values are inode numbers

    /// Table of facts of use of write() by file descriptors.
    bool[int] wrTable;

    VitisFileInfo[int] fdTable;

    DDTable ddTable;

    immutable int[char] permBase;

public:

    // extended name view: ":iX:basename"
    // ":i:" is 3 symbols
    // X is a 46-bit integer inode number (max. 14 symbols)
    // special postfix (\\note or \\names) has max. 6 symbols
    // 255 - (14 + 3 + 6) = 232
    static immutable ssize_t maxNameLength = 232;

    this(string fsFile, string primaryMountPoint) {
        permBase = ['r': 2, 'w': 1, 'x': 0];

        this.fsFile = getAbsolutePath(fsFile);
        this.inodesStorage = buildPath(primaryMountPoint, ".vitis");
        this.pseudoStorage = buildPath(this.inodesStorage, attrPCPrefix);

        this.ddTable = new DDTable;
    }

    override void initialize(fuse_conn_info* conn, fuse_config* cfg) {
        cfg.use_ino = 1;
        cfg.attr_timeout = 0.0;
        cfg.entry_timeout = 0.0;
        cfg.negative_timeout = 0.0;
        cfg.ac_attr_timeout_set = 0;
        cfg.ac_attr_timeout = 0.0;
        immutable rootCategory = getRootCategorySpecDir();
        this.inoGenerator = new InoGenerator(this.inodesStorage);
        if (!exists(rootCategory)) {
            std.file.mkdirRecurse(this.inodesStorage);
            prepareInodeDir("", VFT.directory, oct!777);  // root directory
            this.mkAttrPseudoCategory("");
            this.mkAttrPseudoCategory("Extension");
            this.mkAttrPseudoCategory("Format");
            this.mkAttrPseudoCategory("Group");
        }
    }

    override void destroy() {
        super.destroy();
    }

    override stat_t getattr(string path, ffi_t* fi = null) {
        path = path.stripSlashes;
        stat_t st;
        ulong tid = thisThreadID();
        if (path.endsWith(repeatedNameEnding)) {
            if (tid !in this.repeatedNameCases) {
                fe(ENOENT);
            } else {
                string specDir;
                ino_t ino = this.repeatedNameCases[tid];
                specDir = buildPath(this.inodesStorage, memInoToDirSeq(ino));
                VitisFileInfo info = makeInfoBySpecDirAndIno(specDir, ino);
                st = makeStat(info, path);
                this.repeatedNameCases.remove(tid);
            }
        } else if (tid in imaginaryLinkCases) {
            st = makeStat(path ~ imLinkEnding);
            this.imaginaryLinkCases.remove(tid);
        } else {
            st = makeStat(path);
        }
        return st;
    }

    override void mkdir(string path, mode_t mode) {
        path = path.stripSlashes;
        fe(!path.isAmong(rpn.operands), EINVAL);
        checkIfPathIsRegular(path);
        fe(!path.endsWith(noteEnding), EACCES);
        prepareInodeDir(path, VFT.directory, mode);
    }

    override void mknod(string path, mode_t mode, dev_t dev) {
        path = stripLeft(path, '/');
        fe(!path.endsWith("/"), EISDIR);
        checkIfPathIsRegular(path);
        fe(!path.endsWith(noteEnding), EACCES);
        auto ftype = cast(VFT)(cast(char)getFileTypeFromFileMode(mode));
        ino_t ino;
        string specDir;
        S!(ino, specDir) = prepareInodeDir(path, ftype, mode);
        scope(failure) inoGenerator.freeInodeNumber(ino);

        string inodePath = getRealFilePath(specDir);
        int ret = unix_fcntl.mknod(inodePath.toStringz, mode, dev);
        fe(ret != -1, errno);
    }

    override void create(string path, mode_t mode, ffi_t* fi) {
        path = stripLeft(path, '/');
        fe(!path.endsWith("/"), EISDIR);
        checkIfPathIsRegular(path, No.validate);

        ino_t ino;
        string specDir;
        int fd;

        VitisFileInfo info;
        info.pft = detectPseudoFileByPath(path);
        final switch(info.pft) {
            case PseudoFileType.note:
                info.fileType = VFT.pseudofile;
                S!(specDir, ino) = getSpecInodeDir(path);
                string noteFile = buildPath(specDir, "note");
                fd = unix_fcntl.creat(noteFile.toStringz, mode);
                fe(fd != -1, errno);
                ino += offsetNotes;
                break;
            case PseudoFileType.imLink:
                fe(EPERM);
                break;
            case PseudoFileType.pathItem:
                fe(EPERM);
                break;
            case PseudoFileType.none:
                checkSymbolsInPath(path);
                info.fileType = VFT.regularFile;
                S!(ino, specDir) = prepareInodeDir(path, VFT.regularFile, mode);
                scope(failure) inoGenerator.freeInodeNumber(ino);
                string filePath = getRealFilePath(specDir);
                fd = unix_fcntl.creat(filePath.toStringz, mode);
                fe(fd != -1, errno);
                collectException(updateExtensionAttr("", path, ino, specDir));
                break;
        }
        info.specDir = specDir;
        info.ino = ino;

        fi.fh = cast(ulong)fd;
        this.wrTable[fd] = false;
        this.fdTable[fd] = info;
        this.inodeTable.addNewOpenInode(ino);
    }

    override void symlink(string path, string newlink) {
        checkIfPathIsRegular(newlink, No.validate);
        newlink = stripLeft(newlink, '/');
        fe(!newlink.endsWith(noteEnding), EACCES);

        ino_t ino;
        string specDir;
        S!(ino, specDir) = prepareInodeDir(newlink, VFT.symlink, oct!777);
        scope(failure) inoGenerator.freeInodeNumber(ino);

        string inodePath = getRealFilePath(specDir);
        std.file.symlink(path, inodePath);
    }

    override string readlink(string path) {
        path = path.stripLeft('/');
        if (isImaginaryLink(path)) {
            ino_t ino = getSpecInodeDir(path)[1];
            return constructUnambiguousPath(ino);
        }
        VitisFileInfo info = findInfoAboutFile(path);
        if (info.pft == PseudoFileType.pathItem) {
            return readPathItem(path, info.specDir, info.ino);
        }
        fe(!info.specDir.empty, EINVAL);
        return std.file.readLink(getRealFilePath(info.specDir));
    }

    override void link(string oldPath, string newPath) {
        oldPath = oldPath.stripSlashes;
        newPath = newPath.stripSlashes;
        fe(oldPath != "" && newPath != "", EINVAL);

        // state of the new path
        fe(!requestIsRelatedToPseudoCategory(newPath), EPERM);
        fe(!isPseudoFile(newPath), EPERM);
        bool nameIsRepeated = checkRepetition(newPath);
        checkSymbolsInPath(newPath);

        // state of old path
        VitisFileInfo info = findInfoAboutFile(oldPath);
        while (info.fileType == VFT.pseudofile) {
            fe(info.pft == PseudoFileType.imLink, EPERM);
            ulong tid = thisThreadID();
            this.imaginaryLinkCases[tid] = info.ino;
            oldPath = oldPath.stripRight(imLinkEnding);
            info = findInfoAboutFile(oldPath);
        }
        fe(info.fileType != VFT.pseudocategory, EPERM);
        checkSymbolsInPath(oldPath);

        // directory loop
        void checkDirLoop(
            string primarySpecDir, ino_t primaryIno, ino_t controlIno
        ) {
            fe(primaryIno != info.ino, ELOOP);
            string currCatFile = buildPath(primarySpecDir, "categories");
            auto cats = DDS.collectInoExcept(currCatFile, info.ino);
            fe(cats.hasValue(), ELOOP);
            foreach(ino_t ino; cats.get()) {
                auto s = buildPath(this.inodesStorage, memInoToDirSeq(ino));
                checkDirLoop(s, ino, controlIno);
            }
        }
        string newPathParentDir = getDirName(newPath);
        VitisFileInfo newPathParentInfo = findInfoAboutFile(newPathParentDir);
        string parSpec = newPathParentInfo.specDir;
        checkDirLoop(parSpec, newPathParentInfo.ino, info.ino);

        // link

        this.inodeTable.lockInodeMutex(info.ino);
        scope(exit) this.inodeTable.unlockInodeMutex(info.ino);

        bool fileIsDir = (info.fileType == VFT.directory);
        string parSub;
        parSub = buildPath(parSpec, fileIsDir ? "subcategories" : "nodes");
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        uint nlink = attrs.get("vitis.stat.nlink").to!uint;
        fe(nlink != LIMIT_NLINKS, EMLINK);
        nlink++;

        string newName = baseName(newPath);
        string categFile = buildPath(info.specDir, "categories");
        DDS.add(categFile, newName, newPathParentInfo.ino);
        scope(failure) DDS.remove(categFile, newName, newPathParentInfo.ino);
        DDS.add(parSub, newName, info.ino);
        scope(failure) DDS.remove(parSub, newName, info.ino);
        attrs.set("vitis.stat.nlink", nlink.to!string);

        updateTime(info);

        if (nameIsRepeated) {
            ulong tid = thisThreadID();
            this.repeatedNameCases[tid] = info.ino;
        }
    }

    override bool access(string path, int mode) {
        path = stripLeft(path, '/');
        VitisFileInfo info = findInfoAboutFile(path);
        if (info.pft != PseudoFileType.none) {
            while (info.pft != PseudoFileType.none) {
                path = path.cutOffSpecEnding();
            }
        } else if (info.pct != PseudoCategoryType.none) {
            if (mode & R_OK) return true;
            if (mode & W_OK) return false;
            if (mode & X_OK) return true;
        }

        string attrsFile = buildPath(getSpecInodeDir(path)[0], "attrs");
        auto fr = AttrControl(attrsFile).readFileRights();
        auto context = getContextMode();
        bool check(char perm) {
            if (!checkAccess(context.uid, context.gid, fr, perm)) {
                errno = EACCES;
                return false;
            }
            return true;
        }
        if ((mode & R_OK) && !check('r')) return false;
        if ((mode & W_OK) && !check('w')) return false;
        if ((mode & X_OK) && !check('x')) return false;
        return true;
    }

    override void rename(string src, string dst, uint flags) {
        scope(success) updateTime(findInfoAboutFile(dst));

        auto info = findInfoAboutFile(src);
        fe(!info.isPseudo(), EPERM);

        if (!dst.endsWith(repeatedNameEnding)) {
            // existing empty directory can be replaced by another
            auto dstInfo = safeFindInfoAboutFile(dst);
            if (dstInfo.hasValue()) {
                fe(info.isDir && dstInfo.isDir, EEXIST);
                auto subcat = buildPath(dstInfo.specDir, "subcategories");
                auto nodes = buildPath(dstInfo.specDir, "nodes");
                fe(DDS.isEmpty(subcat) && DDS.isEmpty(nodes), ENOTEMPTY);
                this.rmdir(dst);
                this.rename(src, dst, flags);
                return;
            }
        }

        checkRepetition(dst);
        Optional!FileFromNameList fromNameList = getFileFromNameList(src);
        if (fromNameList.hasValue()) {
            src = fromNameList.transformToExactPath(info.ino);
        }
        fe(!isFileFromPseudoCategory(src), EPERM);
        VitisFileInfo dstParentInfo = findInfoAboutFile(dirName(dst));
        fe(dstParentInfo.isDir(), EPERM);

        checkSymbolsInPath(src);
        checkSymbolsInPath(dst);

        src = src.stripSlashes;
        dst = dst.stripSlashes;
        fe(!src.empty && !dst.empty, EBUSY);
        if (flags == RENAME_EXCHANGE) {
            exchange(src, dst, info.ino);
            return;
        }
        fe(flags == 0, EINVAL);

        string specDir = info.specDir;
        ino_t ino = info.ino;
        string catFile = buildPath(specDir, "categories");
        string oldName = baseName(src);
        auto oldParentInfo = findInfoAboutFile(dirName(src));
        string newName = baseName(dst);
        auto newParentInfo = findInfoAboutFile(dirName(dst));

        DDS.findAndReplace(
            catFile, oldName, oldParentInfo.ino, newName, newParentInfo.ino
        );
        string oldParentCatFile, newParentCatFile;
        auto sub = info.fileType == VFT.directory ? "subcategories" : "nodes";
        oldParentCatFile = buildPath(oldParentInfo.specDir, sub);
        newParentCatFile = buildPath(newParentInfo.specDir, sub);
        if (oldParentCatFile == newParentCatFile) {  // category isn't changed
            DDS.findAndReplace(oldParentCatFile, oldName, ino, newName, ino);
        } else {
            DDS.remove(oldParentCatFile, oldName, ino);
            DDS.add(newParentCatFile, newName, ino);
        }
    }

    void exchange(string path1, string path2, ino_t ino) {
        // It's allows to have files with the same name in the same directory,
        // so these operations are allowed:
        this.rename(path1, path2 ~ repeatedNameEnding, 0);
        string path2WithIno = constructRepeatedName(ino, path2);
        this.rename(path2WithIno, path1 ~ repeatedNameEnding, 0);
    }

    override void rmdir(string path) {
        path = stripLeft(path, '/');
        fe(path != "", EBUSY);
        fe(!isFileRequestByNumber(path), EINVAL);

        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EACCES);
        fe(info.fileType == VFT.directory, ENOTDIR);
        string nodesFile = buildPath(info.specDir, "nodes");

        Optional!FileFromNameList fromNameList = getFileFromNameList(path);
        if (fromNameList.hasValue()) {
            path = fromNameList.transformToExactPath(info.ino);
        }

        VitisFileInfo parentInfo = findInfoAboutFile(dirName(path));
        auto parentSubcatFile = buildPath(parentInfo.specDir, "subcategories");
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        uint n = attrs.get("vitis.stat.nlink").to!uint - 1;
        fe(n > 1 || DDS.isEmpty(nodesFile), ENOTEMPTY);
        n == 1 ? deleteFile(info) : attrs.set("vitis.stat.nlink", n.to!string);
        DDS.remove(parentSubcatFile, baseName(path), info.ino);
        updateMCTime(parentInfo, getTimeSpec());
    }

    override void unlink(string path) {
        path = path.stripLeft('/');
        fe(!isFileRequestByNumber(path), EINVAL);

        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EACCES);
        if (info.fileType == VFT.directory) {
            this.rmdir(path);
            return;
        }
        final switch(info.pft) {
            case PseudoFileType.note:
                std.file.remove(buildPath(info.specDir, "note"));
                return;
            case PseudoFileType.imLink:
                fe(ENOENT);
                break;
            case PseudoFileType.pathItem:
                fe(EPERM);
                break;
            case PseudoFileType.none:
                break;
        }
        string categFile = buildPath(info.specDir, "categories");

        Optional!FileFromNameList fromNameList = getFileFromNameList(path);
        if (fromNameList.hasValue()) {
            path = fromNameList.transformToExactPath(info.ino);
        }

        string parentDir = getDirName(path);
        VitisFileInfo parentInfo = findInfoAboutFile(parentDir);
        string name = baseName(path);
        DDS.remove(categFile, name, parentInfo.ino);
        auto parentNodesFile = buildPath(parentInfo.specDir, "nodes");
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        updateTime(info);
        uint n = attrs.get("vitis.stat.nlink").to!uint - 1;
        n == 1 ? deleteFile(info) : attrs.set("vitis.stat.nlink", n.to!string);
        DDS.remove(parentNodesFile, name, info.ino);
    }

    override void open(string path, ffi_t* fi) {
        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.directory, EISDIR);
        fe(info.fileType != VFT.pseudocategory, EISDIR);

        void handleSymlink() {
            fe(!cast(bool)(fi.flags & O_NOFOLLOW), ELOOP);
            this.readLinkRec(path, info);
            fe(info.fileType != VFT.directory, EISDIR);
            fe(info.fileType != VFT.pseudocategory, EISDIR);
        }
        string dsPath;
        if (info.fileType == VFT.symlink) {
            handleSymlink();
        } else if (info.fileType == VFT.pseudofile) {
            final switch(info.pft) {
                case PseudoFileType.note:
                    dsPath = buildPath(info.specDir, "note");
                    fe(exists(dsPath), ENOENT);
                    break;
                case PseudoFileType.imLink:
                    path = path.stripRight(imLinkEnding);
                    handleSymlink();
                    dsPath = getRealFilePath(info.specDir);
                    break;
                case PseudoFileType.pathItem:
                    handleSymlink();
                    break;
                case PseudoFileType.none:
                    fe(EIO);
            }
        } else {
            dsPath = getRealFilePath(info.specDir);
        }

        string attrsFile = buildPath(info.specDir, "attrs");
        auto fr = AttrControl(attrsFile).readFileRights();
        auto context = getContextMode();
        foreach(flag; openFlagsForWriting) {
            if (fi.flags & flag) {
                fe(checkAccess(context.uid, context.gid, fr, 'w'), EACCES);
            }
        }
        int fd = openFile(dsPath, fi.flags);
        fi.fh = cast(ulong)fd;
        this.wrTable[fd] = false;
        this.fdTable[fd] = info;
        this.inodeTable.addNewOpenInode(info.ino);
    }

    override int write(
        string path, const ubyte* data, size_t len, off_t offset, ffi_t* fi
    ) {
        fe(len <= int.max, EOVERFLOW);
        int fd = cast(int)fi.fh;
        ino_t ino = fdTable[fd].ino;
        inodeTable.lockInodeMutex(ino);
        scope(exit) inodeTable.unlockInodeMutex(ino);
        int nbytes = cast(int)unix_unistd.pwrite(fd, data, len, offset);
        fe(nbytes != -1, errno);
        this.wrTable[fd] = true;
        updateTime(this.fdTable[fd]);
        return nbytes;
    }

    override int read(
        string path, ubyte* buf, size_t len, off_t offset, ffi_t* fi
    ) {
        fe(len <= int.max, EOVERFLOW);
        int fd = cast(int)fi.fh;
        memset(buf, 0, len);
        int nbytes = cast(int)unix_unistd.pread(fd, buf, len, offset);
        fe(nbytes != -1, errno);
        updateTime(this.fdTable[fd]);
        return nbytes;
    }

    override off_t lseek(string path, off_t offset, int whence, ffi_t* fi) {
        int fd = cast(int)fi.fh;
        off_t ret = unix_unistd.lseek(fd, offset, whence);
        fe(ret != cast(off_t)(-1), errno);
        return ret;
    }

    override void truncate(string path, off_t length, ffi_t* fi) {
        int fd = (fi == null) ? 0 : cast(int)(fi.fh);
        auto info = (fd == 0) ? findInfoAboutFile(path) : this.fdTable[fd];
        if (info.fileType == VFT.symlink) {
            this.readLinkRec(path, info);
        }
        fe(info.fileType == VFT.regularFile, EINVAL);
        if (fd == 0) {
            string dsPath = getRealFilePath(info.specDir);
            amalthea.fs.truncate(dsPath, length);
        } else {
            int ret = unix_unistd.ftruncate(fd, length);
            fe(ret != -1, errno);
        }
        updateTime(info);
    }

    override void flush(string path, ffi_t* fi) {}

    override void release(string path, ffi_t* fi) {
        errno = 0;
        int fd = cast(int)fi.fh;

        VitisFileInfo info = this.fdTable[fd];
        stat_t st = makeStat(info, path);
        bool itIsPseudofile = info.fileType == VFT.pseudofile;
        if (st.st_size && !itIsPseudofile && wrTable[fd]) {
            collectException(updateFormatAttrs(path, st.st_ino));
        }

        int ret = unix_unistd.close(fd);
        fe(ret != -1, errno);
        this.wrTable.remove(fd);
        this.fdTable.remove(fd);
        inodeTable.removeOpenInode(st.st_ino);
    }

    override void opendir(string path, ffi_t* fi) {
        path = path.stripSlashes;
        VitisFileInfo info = findInfoAboutFile(path);
        if (info.fileType == VFT.symlink) {
            this.readLinkRec(path, info);
        }
        bool fileIsDir = info.fileType == VFT.directory;
        bool fileIsPseudoCat = info.fileType == VFT.pseudocategory;
        fe(fileIsDir || fileIsPseudoCat, ENOTDIR);
        string[] files;
        if (fileIsPseudoCat) {
            NameAndIno[] pcContent = readPseudoCategory(path, info);
            pcContent = completeNamesWithNumbersIfNeeded(pcContent);
            files ~= pcContent.map!(a => a.name).array;
        } else {
            string subFile = buildPath(info.specDir, "subcategories");
            string nodFile = buildPath(info.specDir, "nodes");
            NameAndIno[] dirs, nodes;
            dirs = DDS.readAll(subFile).sort!((a, b) => a.name < b.name).array;
            nodes = DDS.readAll(nodFile).sort!((a, b) => a.name < b.name).array;
            dirs = completeNamesWithNumbersIfNeeded(dirs);
            nodes = completeNamesWithNumbersIfNeeded(nodes);
            files = dirs.map!(a => a.name).array;
            files ~= nodes.map!(a => a.name).array;
            updateTime(info);
        }
        fi.fh = cast(ulong)(ddTable.makeDirectoryDescriptor(info.ino, files));
    }

    override string[] readdir(string path, ffi_t* fi) {
        path = path.stripSlashes;
        int descriptor = cast(int)fi.fh;
        auto pct = detectPseudoCategoryByPath(path);
        string[] points;
        if (pct == PCT.none || pct == PCT.attribute) {
            points = [".", ".."];
        }
        return points ~ ddTable.takeFiles(descriptor);
    }

    override void releasedir(string path, ffi_t* fi) {
        ddTable.freeDirectoryDescriptor(cast(int)fi.fh);
    }

    override void chmod(string path, mode_t mode, ffi_t* fi) {
        path = stripLeft(path, '/');
        int fd = (fi == null) ? 0 : cast(int)(fi.fh);
        VitisFileInfo info = (fd == 0) ? findInfoAboutFile(path) : fdTable[fd];
        fe(info.fileType != VFT.pseudocategory, EPERM);
        fe(info.fileType != VFT.pseudofile, EPERM);
        if (info.fileType == VFT.symlink) {
            this.readLinkRec(path, info);
        }

        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        auto fr = attrs.readFileRights();
        auto c = getContextMode();
        fe(c.uid == fr.owner || c.gid == fr.group || c.uid == 0, EACCES);
        mode &= antitypeMask;
        attrs.set("vitis.filemode", format!"%012b"(mode));

        updateTime(info);
    }

    override void chown(string path, uid_t uid, gid_t gid, ffi_t* fi) {
        path = stripLeft(path, '/');

        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EACCES);
        fe(info.fileType != VFT.pseudofile, EACCES);

        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        auto fr = attrs.readFileRights();
        auto c = getContextMode();
        fe(c.uid == fr.owner || c.gid == fr.group || c.uid == 0, EACCES);
        if (uid != cast(uid_t)(-1)) {
            attrs.set("vitis.userid", to!string(uid));
        }
        if (gid != cast(gid_t)(-1)) {
            attrs.set("vitis.groupid", to!string(gid));
        }

        updateTime(info);
    }

    override void fsync(string path, int datasync, ffi_t* fi) {
        int fd = cast(int)fi.fh;
        int ret = datasync ? unix_unistd.fdatasync(fd) : unix_unistd.fsync(fd);
        fe(ret != -1, errno);
    }

    override void utimens(string path, ref const(timespec)[2] tv, ffi_t* fi) {
        auto flags = fi != null ? fi.flags : 0;
        enum AT_SYMLINK_NOFOLLOW = 0x100;

        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EPERM);
        if (info.fileType == VFT.pseudofile) {
            final switch(info.pft) {
                case PseudoFileType.note:
                    auto noteFile = buildPath(info.specDir, "note").toStringz;
                    enum AT_FDCWD = -100;
                    if (-1 == utimensat(AT_FDCWD, noteFile, tv, flags)) {
                        fe(errno);
                    }
                    return;
                case PseudoFileType.imLink:
                    info = findInfoAboutFile(path.stripRight(imLinkEnding));
                    break;
                case PseudoFileType.pathItem:
                    fe(EPERM);
                    break;
                case PseudoFileType.none:
                    fe(EIO);
            }
        }

        bool fileIsSymlink = info.fileType == VFT.symlink;
        if (fileIsSymlink && flags != AT_SYMLINK_NOFOLLOW) {
            this.readLinkRec(path, info);
        }

        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        timespec newAtime = tv[0];
        timespec newMtime = tv[1];
        timespec currTime = getTimeSpec();
        if (newAtime.tv_nsec == UTIME_NOW) newAtime = currTime;
        if (newMtime.tv_nsec == UTIME_NOW) newMtime = currTime;

        if (newAtime.tv_nsec != UTIME_OMIT) {
            attrs.set("vitis.stat.atime", newAtime);
        }
        if (newMtime.tv_nsec != UTIME_OMIT) {
            attrs.set("vitis.stat.mtime", newMtime);
        }
        attrs.set("vitis.stat.ctime", currTime);
    }

    override void flock(string path, ffi_t* fi, int op) {
        int fd = cast(int)fi.fh;
        int ret = linux_flock(fd, op);
        fe(ret != -1, errno);
    }

    override void fallocate(
        string path, int mode, off_t offset, off_t len, ffi_t* fi
    ) {
        int fd = cast(int)fi.fh;
        int ret = linux_fallocate(fd, mode, offset, len);
        fe(ret != -1, errno);
    }

    override void setxattr(string path, string key, string value, int flags) {
        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EACCES);
        fe(info.fileType != VFT.pseudofile, EACCES);
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        if (key.startsWith("vitis.")) {
            fe(key == "vitis.canon_name", EPERM);
        } else {
            fe(key.startsWith("user."), ENOTSUP);
        }
        if (flags == 0) {
            attrs.set(key, value);
        } else {
            bool keyDoesNotExist = !attrs.optGet(key).hasValue();
            if (flags == XATTR_CREATE) {
                fe(keyDoesNotExist, EEXIST);
            } else if (flags == XATTR_REPLACE) {
                fe(!keyDoesNotExist, ENODATA);
            } else {  // wrong flags
                fe(EINVAL);
            }
            attrs.set(key, value);
        }
        updateTime(info);
    }

    override string getxattr(string path, string key) {
        fe(key.startsWith("user.") || key.startsWith("vitis."), ENOTSUP);
        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EACCES);
        fe(info.fileType != VFT.pseudofile, EACCES);
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        auto result = attrs.optGet(key);
        fe(result.hasValue(), ENODATA);
        return result.get();
    }

    override string[] listxattr(string path) {
        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EPERM);
        fe(info.fileType != VFT.pseudofile, EPERM);
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        return attrs.getAllKeys();
    }

    override void removexattr(string path, string key) {
        fe(!key.startsWith("vitis."), EPERM);
        VitisFileInfo info = findInfoAboutFile(path);
        fe(info.fileType != VFT.pseudocategory, EACCES);
        fe(info.fileType != VFT.pseudofile, EACCES);
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        fe(attrs.optGet(key).hasValue(), ENODATA);
        fe(!doesExceptionCatch(attrs.get(key)), ENODATA);
        attrs.remove(key);
        updateTime(info);
    }

    /*
     * The 'Vitis' filesystem cannot give correct information about number
     * of nodes in the struct fields 'f_files', 'f_ffree, 'f_favail'.
     */
    override statvfs_t statfs(string) {
        statvfs_t vfs;
        int ret = unix_statvfs.statvfs(this.inodesStorage.toStringz, &vfs);
        fe(ret != -1, errno);
        vfs.f_namemax = this.maxNameLength;
        return vfs;
    }

    debug
    override void handleFuseException(FuseException e) nothrow {
        safeWriteln(e);
    }

    /// Handler for unknown errors.
    override void handleException(Exception e) nothrow {
        safeWriteln(e.msg);
    }

protected:

    /**
     * Checks file path for invalid characters.
     * Throws an exception with EINVAL on error.
     */
    void checkSymbolsInPath(string p) {
        p.each!(e => fe(!canFind(this.unacceptableSymbols, e), EINVAL));
    }

    /**
     * Throws an exception if the requested path is invalid.
     * Ending for repeated names is allowed.
     * Notes also are allowed.
     */
    void checkIfPathIsRegular(string path, Flag!"validate" v = Yes.validate) {
        if (v) {
            checkSymbolsInPath(path);
        }
        fe(!baseName(path).startsWith(":i"), EINVAL);
        fe(!baseName(path).startsWith(":c"), EINVAL);
        fe(!path.startsWith(attrPCPrefix), EACCES);
        fe(!path.endsWith(fileNameListEnding), EACCES);
        fe(!path.endsWith(expEnding), EACCES);
        fe(!path.endsWith(imLinkEnding), EPERM);
        fe(baseName(path).length <= this.maxNameLength, ENAMETOOLONG);
    }

    /**
     * Modifies path: deletes repeated name ending.
     *
     * Throws an exception if the requested path is existent and special
     * special ending isn't used.
     */
    bool checkRepetition(ref string path) {
        bool repeatedName;
        while (path.endsWith(repeatedNameEnding)) {
            repeatedName = true;
            path = path[0 .. $-repeatedNameEnding.length];
        }
        string category = dirName(path);
        string fileName = baseName(path);
        fe(!isNameInCategory(category, fileName) || repeatedName, EEXIST);
        return repeatedName;
    }

    void readLinkRec(ref string path, ref VitisFileInfo info) {
        typeof(ELOOP_LIMIT) counter = 1;
        while (info.fileType == VFT.symlink) {
            counter++;
            fe(counter <= ELOOP_LIMIT, ELOOP);
            path = this.readlink(path);
            info = findInfoAboutFile(path);
        }
    }

    string getRealFilePath(string specDir) nothrow {
        return buildPath(specDir, "file");
    }

    string buildAttrPseudoCategoryPath(string apc) nothrow
    in {
        assert(apc.startsWith(attrPCPrefix));
    } do {
        apc = apc.stripSlashes;
        auto al = attrPCPrefix.length;
        return buildPath(this.pseudoStorage, apc[al .. $].stripSlashes);
    }

    NameAndIno[] readAttrPseudoCategory(string path) {
        NameAndIno[] files;
        auto pcPath = buildAttrPseudoCategoryPath(path);
        FileEntry[] dirs = getDirectories(pcPath);
        // attribute category contains either directories or regular files
        if (dirs.length > 0) {
            foreach(d; dirs) {
                string name = baseName(d.path);
                auto p = buildPath(pcPath, name);
                auto ino = buildPath(p, "st_ino").readText.to!ino_t;
                files ~= NameAndIno(name, ino);
            }
        } else {
            FileEntry[] nodes = getFiles(pcPath);
            ino_t[] numbers;
            foreach(node; nodes) {
                string name = baseName(node.path);
                if (name != "st_ino") {
                    numbers ~= to!ino_t(name);
                }
            }
            files ~= getNamesByInoNumbers(numbers);
        }
        return files;
    }

    NameAndIno[] readNameList(string specDir, ino_t ino) {
        string catFile = buildPath(specDir, "categories");
        NameAndIno[] all = DDS.readAll(catFile);
        return remakeAsFileNameList(all, ino);
    }

    Tuple!(NameAndIno, string)[] readPathList(string specDir, ino_t relIno) {
        string[] chains;
        void makeDirChain(string catFile, string dir) {
            NameAndIno[] cats = DDS.readAll(catFile);
            string currSpecDir, currCatFile;
            if (cats.length == 0) {
                chains ~= dir.idup;
            }
            foreach(nameAndIno; cats) {
                string name = nameAndIno.name;
                ino_t ino = nameAndIno.ino;
                string currDir = buildPath(name, dir);
                if (ino == 1) {  // root
                    chains ~= currDir.idup;
                    continue;
                }
                currSpecDir = buildPath(inodesStorage, memInoToDirSeq(ino));
                currCatFile = buildPath(currSpecDir, "categories");
                makeDirChain(currCatFile, currDir.idup);
            }
        }
        string catFile = buildPath(specDir, "categories");
        makeDirChain(catFile, "");
        chains.sort();
        Tuple!(NameAndIno, string)[] result;
        foreach(i, symlinkDest; chains) {
            string name = to!string(i + 1);
            ino_t pathItemIno = relIno + offsetPathList + i;
            result ~= tuple(NameAndIno(name, pathItemIno), symlinkDest);
        }
        return result;
    }

    string readPathItem(
        string path, string parentFileSpecDir, ino_t parentFileIno
    ) {
        path = path.stripSlashes;
        Tuple!(NameAndIno, string)[] list = readPathList(
            parentFileSpecDir, parentFileIno
        );
        string listPath = path.cutOffSpecEnding ~ filePathListEnding;
        foreach(elem; list) {
            string name = elem[0].name;
            string linkDest = elem[1];
            string elemFullPath = buildPath(listPath, name);
            if (path == elemFullPath) {
                return linkDest;
            }
        }
        fe(ENOENT);
        return null;
    }

    NameAndIno[] readPseudoCategory(string path, VitisFileInfo info) {
        NameAndIno[] files;
        with(info) files = predSwitch(
            info.pct,
            PCT.expression, calculateExpression(path),
            PCT.attribute,  readAttrPseudoCategory(path),
            PCT.nameList,   readNameList(specDir, ino),
            PCT.pathList,   readPathList(specDir, ino).map!(a => a[0]).array,
            null
        );
        files.sort!((a, b) => stripNodeId(a.name) < stripNodeId(b.name));
        return files;
    }

    /**
     * Gets new inode number and creates new special inode directory.
     * Attention! A `umask` will be applied to the set mode out of the function.
     */
    Tuple!(ino_t, string) prepareInodeDir(string path, VFT ftype, mode_t mode)
    in {
        assert(!path.startsWith('/'));
    } do {
        if (!path.empty) { // if this path is not root directory
            checkRepetition(path);
        }
        ino_t ino;
        string specDir;
        S!(ino, specDir) = inoGenerator.generateInodeNumber();
        scope(failure) inoGenerator.freeInodeNumber(ino);
        string fileName = path.empty ? "/" : baseName(path);
        string category = getDirName(path);
        string attrsFile = buildPath(specDir, "attrs");
        auto attrs = AttrControl(attrsFile);
        auto ts = getTimeSpec();
        attrs.initContent(fileName, ftype, ts, mode);
        string catFile = buildPath(specDir, "categories");
        DDS.initialize(catFile);
        if (ftype == VFT.directory) {
            string nodesFile = buildPath(specDir, "nodes");
            string subcatFile = buildPath(specDir, "subcategories");
            DDS.initialize(nodesFile);
            DDS.initialize(subcatFile);
        }
        if (!path.empty) { // if this path is not root directory
            // write category to binary registry
            VitisFileInfo parentInfo = findInfoAboutFile(category);
            auto context = getContextMode();
            enforcePerm(parentInfo.specDir, context.uid, context.gid, 'w');
            DDS.add(catFile, fileName, parentInfo.ino);
            bool fileIsDir = (ftype == VFT.directory);
            string parSub = parentInfo.specDir;
            parSub = buildPath(parSub, fileIsDir ? "subcategories" : "nodes");
            DDS.add(parSub, fileName, ino);
            updateMCTime(parentInfo, ts);
        }
        return tuple(ino, specDir);
    }

    void deleteInodeFromAttrPseudoCategories(string attrsFile, ino_t ino) {
        auto attrs = AttrControl(attrsFile);
        string strIno = ino.to!string;

        void del(string extAttr, string type) {
            string value = attrs.get(extAttr);
            if (!value.empty) {
                string p = buildPath(this.pseudoStorage, type, value, strIno);
                if (exists(p)) {
                    std.file.remove(p);
                }
            }
        }
        del("vitis.apc.format", "Format");
        del("vitis.apc.group", "Group");
        del("vitis.apc.extension", "Extension");
    }

    string getRootCategorySpecDir() nothrow {
        auto nlevels = InoGenerator.nlevels;
        // "00/00/00/00"
        string dirs = "00".repeat.take(nlevels-1).array.join("/");
        return buildPath(this.inodesStorage, dirs, "01");
    }

    void enforcePerm(
        string specialDir, uid_t currentUser, gid_t currentGroup, char perm='x'
    ) {
        auto attrs = AttrControl(buildPath(specialDir, "attrs"));
        auto fr = attrs.readFileRights();
        fe(checkAccess(currentUser, currentGroup, fr, perm), EACCES);
    }

    /**
     * Gets full path to a special node directory in datastorage
     * for required existent node or category.
     * Works for regular paths, files in attribute pseudocategories,
     * files from expressions, pseudofiles.
     */
    Tuple!(string, ino_t) getSpecInodeDir(string path) {
        path = path.stripSlashes;
        string specDir;
        ino_t ino = 0;

        if (isFileRequestByNumber(path)) {
            ino = extractNodeId(path);
            specDir = buildPath(this.inodesStorage, memInoToDirSeq(ino));
            return tuple(specDir, ino);
        }

        // check if file is in attribute pseudocategory
        if (path.startsWith(attrPCPrefix)) {
            auto specDirAndINo = findFileInAttrPseudoCategory(path);
            fe(specDirAndINo[1] != 0, ENOENT);
            return specDirAndINo;
        }

        // file from expression
        string[] expressionAndFile = path.split(expEnding ~ "/");
        if (expressionAndFile.length == 2) {
            string expression = expressionAndFile[0];
            string fileName = expressionAndFile[1];
            auto namesAndNumbers = calculateExpression(expression);
            foreach(NameAndIno pair; namesAndNumbers) {
                if (pair.name != fileName) continue;
                specDir = buildPath(inodesStorage, memInoToDirSeq(pair.ino));
                return tuple(specDir, pair.ino);
            }
            fe(ENOENT);
        }

        // file from name list (like "Poetry/yeats.txt\names/:c123:yeats.txt")
        Optional!FileFromNameList fromNameList = getFileFromNameList(path);
        // contains fields like "Poetry/yeats.txt" and ":c123:yeats.txt"
        if (fromNameList.hasValue()) {
            S!(specDir, ino) = getSpecInodeDir(fromNameList.originalFilePath);
            auto namesAndNumbers = readNameList(specDir, ino);
            foreach(NameAndIno pair; namesAndNumbers) {
                if (pair.name != fromNameList.subFileName) continue;
                return tuple(specDir, ino);
            }
            fe(ENOENT);
        }

        // root
        string rootCategory = getRootCategorySpecDir();
        if (path.startsWith(":i1:")) {
            path = path.stripLeft(":i1:");
            path = path.stripLeft('/');
        }
        if (path.empty || path == ".") {
            return tuple(rootCategory, rootIno);
        }
        // pseudofile turns into regular path
        auto pft = detectPseudoFileByPath(path);
        while (pft != PseudoFileType.none) {
            path = cutOffSpecEnding(path);
            pft = detectPseudoFileByPath(path);
        }
        // for regular paths
        string[] pathParts = path.split("/");
        string ddsPath = buildPath(rootCategory, "subcategories");
        specDir = rootCategory;
        auto context = getContextMode();
        foreach(i, name; pathParts) {
            bool thisElementIsLast = (i + 1 == pathParts.length);
            fe(exists(ddsPath), ENOTDIR);
            ino_t[] allMatches = DDS.findName(ddsPath, name);
            if (thisElementIsLast) {  // node or category
                ddsPath = buildPath(specDir, "nodes");
                ino_t[] allMatchesInNodes = DDS.findName(ddsPath, name);
                allMatches ~= allMatchesInNodes;
            }
            fe(allMatches.length != 0, ENOENT);
            fe(allMatches.length == 1, EFAULT);
            ino = allMatches[0];
            specDir = buildPath(this.inodesStorage, memInoToDirSeq(ino));
            if (!thisElementIsLast) {
                ddsPath = buildPath(specDir, "subcategories");
                enforcePerm(specDir, context.uid, context.gid);
            }
        }
        return tuple(specDir, ino);
    }

    bool isNameInCategory(string category, string name) {
        try {
            string specDir;
            ino_t ino;
            S!(specDir, ino) = getSpecInodeDir(category);
            string subCats = buildPath(specDir, "subcategories");
            string nodes = buildPath(specDir, "nodes");
            return DDS.findName(subCats, name) || DDS.findName(nodes, name);
        } catch (Exception e) {
            return false;
        }
    }

    VFT readFileTypeWithSpecDir(string specDir) {
        auto attrs = AttrControl(buildPath(specDir, "attrs"));
        string typeAttr = attrs.get("vitis.filetype");
        fe(typeAttr.length == 1, EIO); // single character
        return cast(VFT)(typeAttr[0]);
    }

    string readCanonNameWithSpecDir(string specDir) {
        auto attrs = AttrControl(buildPath(specDir, "attrs"));
        return attrs.get("vitis.canon_name");
    }

    bool isFileRequestByNumber(string request) nothrow {
        request = request.stripSlashes;
        if (request.startsWith(":i") && request.endsWith(":")) {
            if (request.indexOf('/') != -1) {
                return false;  // complicated expression, like :i2:/:i3:
            }
            try {
                return cast(bool) extractNodeId(request);
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }

    Optional!VitisFileInfo createInfoAboutPseudoCategory(string path) {
        auto info = optional!VitisFileInfo(VitisFileInfo.init);
        info.fileType = VFT.pseudocategory;
        if (path.startsWith(attrPCPrefix)) {
            auto pcPath = buildAttrPseudoCategoryPath(path);
            if (exists(pcPath) && isDir(pcPath)) {
                info.ino = buildPath(pcPath, "st_ino").readText.to!ino_t;
                info.pct = PseudoCategoryType.attribute;
                return info;
            }  // else it is file from attribute pseudocategory
        } else if (isNameListRequest(path)) {
            string specDir;
            ino_t ino;
            S!(specDir, ino) = getSpecInodeDir(cutOffSpecEnding(path));
            info.specDir = specDir;
            info.ino = offsetNameLists + ino;
            info.pct = PseudoCategoryType.nameList;
            return info;
        } else if (isExpression(path)) {
            fe(isValidExpression(path), ENOENT);
            info.ino = offsetExpressions + crc32(path);
            info.pct = PseudoCategoryType.expression;
            return info;
        } else if (isPathListRequest(path)) {
            string specDir;
            ino_t ino;
            S!(specDir, ino) = getSpecInodeDir(cutOffSpecEnding(path));
            info.specDir = specDir;
            info.ino = offsetPathList + ino;
            info.pct = PseudoCategoryType.pathList;
            return info;
        }
        return optional!VitisFileInfo();
    }

    Optional!VitisFileInfo createInfoAboutPseudoFile(string path) {
        PseudoFileType pft = detectPseudoFileByPath(path);
        if (pft == PseudoFileType.none) {
            return optional!VitisFileInfo();
        }
        string specDir;
        ino_t ino;
        try {
            S!(specDir, ino) = getSpecInodeDir(path);
        } catch (FuseException e) {
            path = path.cutOffSpecEnding;
            if (isPseudoCategory(path) || isPseudoFile(path)) {
                fe(EPERM);
            }
            fe(e.err);
        }
        VitisFileInfo info;
        info.fileType = VFT.pseudofile;
        info.specDir = specDir;
        info.ino = ino;
        info.pft = pft;
        final switch(pft) {
            case PseudoFileType.note:
                string noteFile = buildPath(specDir, "note");
                fe(exists(noteFile), ENOENT);
                info.ino += offsetNotes;
                break;
            case PseudoFileType.imLink:
                info.ino += offsetImaginaryLinks;
                break;
            case PseudoFileType.pathItem:
                info.ino += offsetPathItem;
                break;
            case PseudoFileType.none: break;  // impossible
        }
        return optional!VitisFileInfo(info);
    }

    bool checkAccess(
        uid_t currentUser,
        gid_t currentGroup,
        ref const FileRights fr,
        char perm = 'r'
    )
    in {
        assert(perm == 'r' || perm == 'w' || perm == 'x');
    } do {
        if (getContextMode().uid == 0 && perm != 'x') {
            return true;
        }
        auto permLShift = this.permBase[perm];
        bool checkWithBitShift(uint shift) {
            return cast(bool)(fr.permissions & (1 << shift << permLShift));
        }
        if (currentUser == fr.owner) {  // user
            return checkWithBitShift(6);
        } else if (currentGroup == fr.group) {  // group
            return checkWithBitShift(3);
        }
        return checkWithBitShift(0);  // others
    }

    VitisFileInfo findInfoAboutFile(string path) {
        path = stripLeft(path, '/');
        auto pcInfo = createInfoAboutPseudoCategory(path);
        if (pcInfo.hasValue()) {
            return pcInfo.get();
        }
        auto pfInfo = createInfoAboutPseudoFile(path);
        if (pfInfo.hasValue()) {
            return pfInfo.get();
        }
        string specDir;
        ino_t ino;
        S!(specDir, ino) = getSpecInodeDir(path);
        return makeInfoBySpecDirAndIno(specDir, ino);
    }

    Optional!VitisFileInfo safeFindInfoAboutFile(string path) {
        try {
            return optional(findInfoAboutFile(path));
        } catch (Exception e) {
            return optional!VitisFileInfo;
        }
    }

    VitisFileInfo makeInfoBySpecDirAndIno(string specDir, ino_t ino) {
        VitisFileInfo result;
        result.fileType = readFileTypeWithSpecDir(specDir);
        result.specDir = specDir;
        result.ino = ino;
        return result;
    }

    uint readNlinkInSpecDir(const ref AttrControl attrs) {
        return attrs.get("vitis.stat.nlink").to!uint;
    }

    void readTimeStampsToStat(const ref AttrControl attrs, ref stat_t st) {
        string[] t;
        void storeTS(string attr, ref time_t secs, ref ulong nsecs)
        in {
            assert(attr.isAmong(AttrControl.tsAttrs));
        } do {
            t = attrs.get(attr).split('|');
            fe(t.length == 2, EIO);
            secs = t[0].to!time_t;
            nsecs = t[1].to!ulong;
        }
        storeTS("vitis.stat.mtime", st.st_mtime, st.st_mtimensec);
        storeTS("vitis.stat.atime", st.st_atime, st.st_atimensec);
        storeTS("vitis.stat.ctime", st.st_ctime, st.st_ctimensec);
    }

    mode_t makeStMode(const ref AttrControl attrs, VFT t) {
        auto fr = attrs.readFileRights();
        mode_t result = fr.permissions;
        final switch(t) {
            case VFT.socket:      result |= S_IFSOCK; break;
            case VFT.symlink:     result |= S_IFLNK; break;
            case VFT.regularFile: result |= S_IFREG; break;
            case VFT.blockDevice: result |= S_IFBLK; break;
            case VFT.directory:   result |= S_IFDIR; break;
            case VFT.charDevice:  result |= S_IFCHR; break;
            case VFT.pipe:        result |= S_IFIFO; break;
            case VFT.pseudocategory: result |= S_IFDIR; break;
            case VFT.pseudofile:     result |= S_IFREG; break;
            case VFT.unknown: break;
        }
        return result;
    }

    stat_t makeStat(string path) {
        path = stripLeft(path, '/');
        VitisFileInfo info = findInfoAboutFile(path);
        return makeStat(info, path);
    }

    stat_t makeStat(VitisFileInfo info, string path) {
        stat_t st;
        switch(info.fileType) {
            case VFT.pseudocategory:
                string ds = this.inodesStorage;
                if (info.pct == PseudoCategoryType.attribute) {
                    ds = buildAttrPseudoCategoryPath(path);
                }
                st = amalthea.fs.getStat(ds);
                st.st_nlink = 0;
                st.st_mode &= noWriteMask;  // disabled writing
                break;
            case VFT.pseudofile:
                st = makeStat(path.cutOffSpecEnding);
                string attrsFile = buildPath(info.specDir, "attrs");
                auto fr = AttrControl(attrsFile).readFileRights();
                st.st_mode = fr.permissions;
                final switch(info.pft) {
                    case PseudoFileType.note:
                        st.st_mode |= S_IFREG;
                        string notePath = buildPath(info.specDir, "note");
                        st.st_size = amalthea.fs.getStat(notePath).st_size;
                        break;
                    case PseudoFileType.imLink:
                        st.st_mode |= S_IFLNK;
                        st.st_mode &= noWriteMask;  // disabled writing
                        st.st_size = constructUnambiguousPath(info.ino).length;
                        break;
                    case PseudoFileType.pathItem:
                        st.st_mode |= S_IFLNK;
                        st.st_mode &= noWriteMask;  // disabled writing
                        auto dest = readPathItem(path, info.specDir, info.ino);
                        st.st_size = dest.length;
                        break;
                    case PseudoFileType.none:
                        fe(EIO);
                }
                break;
            default:  // other file types, non-pseudo
                if (info.fileType != VFT.directory) {
                    string inode = getRealFilePath(info.specDir);
                    st.st_size = amalthea.fs.getStat(inode).st_size;
                } else {
                    st.st_size = 0; // for directories
                }
                auto ac = AttrControl(buildPath(info.specDir, "attrs"));
                st.st_mode = makeStMode(ac, info.fileType);
                st.st_nlink = readNlinkInSpecDir(ac);
                auto fr = ac.readFileRights();
                st.st_uid = fr.owner;
                st.st_gid = fr.group;
                readTimeStampsToStat(ac, st);
                break;
        }
        st.st_ino = info.ino;
        return st;
    }

    NameAndIno[] getNodes(ino_t catIno) {
        string specDir = buildPath(this.inodesStorage, memInoToDirSeq(catIno));
        string nodesFile = buildPath(specDir, "nodes");
        return DDS.readAll(nodesFile);
    }

    NameAndIno[] getSubcategories(ino_t catIno, Flag!"recurse" r = No.recurse) {
        string specDir = buildPath(this.inodesStorage, memInoToDirSeq(catIno));
        string subcatFile = buildPath(specDir, "subcategories");
        NameAndIno[] subcategories = DDS.readAll(subcatFile);
        if (r == No.recurse) {
            return subcategories;
        }
        NameAndIno[] result = subcategories.dup;
        foreach(subcat; subcategories) {
            // todo: think about stack overflow
            result ~= getSubcategories(subcat.ino, Yes.recurse);
        }
        return result;
    }

    NameAndIno[] readCategory(string category) {
        VitisFileInfo info = findInfoAboutFile(category);
        fe(info.fileType == VFT.directory, EIO);
        string nodesFile = buildPath(info.specDir, "nodes");
        NameAndIno[] dirs = getSubcategories(info.ino, Yes.recurse);
        NameAndIno[] nodes = DDS.readAll(nodesFile);
        foreach(dir; dirs) {
            nodes ~= getNodes(dir.ino);
        }
        return nodes;
    }

    bool isNameListRequest(string request) nothrow {
        return request.endsWith(fileNameListEnding);
    }

    bool isPathListRequest(string request) nothrow {
        return request.endsWith(filePathListEnding);
    }

    bool isExpression(string request) nothrow {
        request = request.stripLeft('/');
        if (!request.endsWith(expEnding)) {
            return false;
        }
        return countElements(request, '/') == 0;
    }

    bool isFileFromPseudoCategory(string request) {
        auto fileInfo = findInfoAboutFile(request);
        auto parentInfo = findInfoAboutFile(dirName(request));
        return !fileInfo.isPseudo() && parentInfo.isPseudoCategory();
    }

    Tuple!(string, ino_t) findFileInAttrPseudoCategory(string request)
    in {
        assert(request.startsWith(attrPCPrefix));
    } do {
        string pcDir = buildAttrPseudoCategoryPath(dirName(request));
        string fileName = baseName(request);
        ino_t extractedIno = 0;
        if (fileName.startsWith(":i")) {
            extractedIno = extractNodeId(fileName);
            fileName = stripNodeId(fileName);
        }

        string specDir;
        try {
            foreach(entry; DirReader(pcDir)) {
                string name = baseName(entry.path);
                if (name == "." || name == ".." || name == "st_ino") {
                    continue;
                }
                ino_t ino = name.to!ino_t;
                specDir = buildPath(this.inodesStorage, memInoToDirSeq(ino));
                string canonName = readCanonNameWithSpecDir(specDir);
                if (fileName == canonName) {
                    if (extractedIno == 0 || ino == extractedIno) {
                        return tuple(specDir, ino);
                    }
                }
            }
        } catch(Exception e) {
            return tuple("", 0UL);
        }
        return tuple("", 0UL);
    }

    bool isPseudoCategory(string request) nothrow {
        return detectPseudoCategoryByPath(request) != PseudoCategoryType.none;
    }

    /// Detects pseudocategory type by path. Existence is not verified.
    PseudoCategoryType detectPseudoCategoryByPath(string path) nothrow {
        if (path.startsWith(attrPCPrefix)) return PseudoCategoryType.attribute;
        if (isExpression(path))            return PseudoCategoryType.expression;
        if (isNameListRequest(path))       return PseudoCategoryType.nameList;
        if (isPathListRequest(path))       return PseudoCategoryType.pathList;
        return PseudoCategoryType.none;
    }

    bool requestIsRelatedToPseudoCategory(string request) {
        request = request.stripLeft("/");
        while (request.endsWith(imLinkEnding)) {
            request = request.stripRight(imLinkEnding);
        }
        if (request.startsWith(attrPCPrefix)) {
            return true;
        } else if (request.endsWith(expEnding)) {
            return true;
        } else if (-1 != request.indexOf(expEnding ~ "/")) {
            return true;
        } else if (request.endsWith(fileNameListEnding)) {
            return true;
        } else if (-1 != request.indexOf(fileNameListEnding ~ "/")) {
            return true;
        }
        return false;
    }

    /// Detects pseudofile type by path. Existence is not verified.
    PseudoFileType detectPseudoFileByPath(string path) nothrow {
        path = path.stripSlashes;
        if (isNote(path)) {
            return PseudoFileType.note;
        }
        if (isImaginaryLink(path)) {
            return PseudoFileType.imLink;
        }
        if (-1 != path.indexOf(filePathListEnding ~ "/")) {
            return PseudoFileType.pathItem;
        }
        return PseudoFileType.none;
    }

    bool isNote(string request) nothrow {
        return request.endsWith(noteEnding);
    }

    bool isImaginaryLink(string request) nothrow {
        return request.endsWith(imLinkEnding);
    }

    bool isPseudoFile(string request) nothrow {
        return isNote(request) || isImaginaryLink(request);
    }

    alias ElementAndType = Tuple!(Variant, "elem", ET, "type");

    /***************************************************************************
    * Returns array of tuples
    * with operands (arrays of inode numbers) or operators (string value)
    * and expression element types.
    */
    ElementAndType[] convertExpressionToRPN(string[] expression) {
        ElementAndType[] result;
        string[] RPN = rpn.toReversePolishNotation(expression);
        ino_t[][string] catFiles;
        foreach (string el; RPN) {
            el = (el != "/") ? el.stripLeft('/') : el;
            ElementAndType newInstance;
            newInstance.type = rpn.etype(el);
            if (newInstance.type == ET.CATEGORY) {
                if (el !in catFiles) {
                    catFiles[el] = readCategory(el).map!(e => e.ino).array;
                }
                newInstance.elem = Variant(catFiles[el]);
            } else {
                newInstance.elem = Variant(el);
            }
            result ~= newInstance;
        }
        return result;
    }

    bool isValidExpression(string rawExpression) {
        if (!isExpression(rawExpression)) {
            return false;
        }
        try {
            string[] elements = rawExpression.split('\\');
            elements = elements[0 .. $-1];  // without suffix part
            string[] RPN = rpn.toReversePolishNotation(elements);
            foreach (string el; RPN) {
                el = (el != "/") ? el.stripLeft('/') : el;
                if (rpn.etype(el) == ET.CATEGORY) {
                    auto fileType = findInfoAboutFile(el).fileType;
                    if (fileType != VFT.directory) {
                        return false;
                    }
                }
            }
        } catch(Exception e) {
            return false;
        }
        return true;
    }

    NameAndIno[] calculateExpression(string rawExpression) {
        fe(isExpression(rawExpression), EINVAL);
        string[] elements = rawExpression.split('\\');
        elements = elements[0 .. $-1];  // without suffix part
        return calculateExpression(elements);
    }

    /***************************************************************************
    * The function takes a mathematical expression with the operations of union,
    * intersection, subtraction in a special vitis-format.
    */
    NameAndIno[] calculateExpression(string[] expression) {
        ElementAndType[] rpnSequence;
        try {
            rpnSequence = convertExpressionToRPN(expression);
        } catch (Exception e) {
            fe(EINVAL);
        }

        ino_t[][] stack;
        foreach(elementOfSequence; rpnSequence) {
            auto exprElement = elementOfSequence.elem;
            auto type = elementOfSequence.type;
            if (type == ET.CATEGORY) {
                stack ~= exprElement.get!(ino_t[]);
                continue;
            } // else it's time to do the calculations
            assert(stack.length >= 2);
            auto values1 = stack[$-2];
            auto values2 = stack[$-1];
            ino_t[] unitedValues = type.predSwitch(
                ET.UNION,        calcUnion(values1, values2),
                ET.INTERSECTION, calcIntersection(values1, values2),
                ET.COMPLEMENT,   calcComplement(values1, values2),
                null
            );
            stack = stack[0 .. $-2];
            stack ~= unitedValues;
        }
        assert(stack.length == 1);
        return getNamesByInoNumbers(stack[0]);  // numbers and names
    }

    NameAndIno[] getNamesByInoNumbers(ino_t[] numbers) {
        size_t[string] fileNames;  // counter
        string[ino_t] numbersAndNames;
        foreach(ino; numbers) {
            string specDir = buildPath(
                this.inodesStorage, memInoToDirSeq(ino)
            );
            string name = readCanonNameWithSpecDir(specDir);
            fileNames[name] = (name !in fileNames) ? 1 : fileNames[name] + 1;
            numbersAndNames[ino] = name;
        }
        foreach(ino; numbers) {
            string name = numbersAndNames[ino];
            if (fileNames[name] > 1) {
                numbersAndNames[ino] = constructRepeatedName(ino, name);
            }
        }
        NameAndIno[] result;
        foreach(ino, name; numbersAndNames) {
            result ~= NameAndIno(name, ino);
        }
        return result;
    }

    NameAndIno[] completeNamesWithNumbersIfNeeded(NameAndIno[] files) {
        auto res = files.dup;
        for (size_t i = 1; i < res.length; i++) {
            if (res[i].name ==res[i-1].name) {
                string name = res[i].name;
                res[i].name = constructRepeatedName(res[i].ino, name);
                res[i-1].name = constructRepeatedName(res[i-1].ino, name);
            }
        }
        return res;
    }

    NameAndIno[] remakeAsFileNameList(NameAndIno[] names, ino_t ino) {
        auto res = names.dup;
        foreach(ref elem; res) {
            elem.name = constructNameInCategory(elem.ino, elem.name);
            elem.ino = ino;
        }
        return res;
    }

    struct FileFromNameList {
        string originalFilePath;
        string subFileName;

        // like ":i123:/:i345:file.txt"
        string transformToExactPath(ino_t fileno) {
            string firstPart = constructUnambiguousPath(
                extractNodeId(this.subFileName)
            );
            string secondPart = constructRepeatedName(
                fileno, baseName(stripNodeId(this.subFileName))
            );
            return buildPath(firstPart, secondPart);
        }
    }

    Optional!FileFromNameList getFileFromNameList(string path) {
        string[] pair = path.split(fileNameListEnding ~ "/");
        if (pair.length == 2) {
            return optional(FileFromNameList(pair[0], pair[1]));
        }
        fe(pair.length <= 2, EINVAL);
        return optional!FileFromNameList;
    }

    void mkAttrPseudoCategory(string name) {
        auto newDir = buildPath(this.pseudoStorage, name);
        safeMkdir(newDir);
        ino_t attrCategIno = offsetAttrPC + crc32(name);
        string stInoFile = buildPath(newDir, "st_ino");
        std.file.write(stInoFile, attrCategIno.to!string);
    }

    void updateAttrPseudoCategory(
        string attr,
        string newValue, string oldValue,
        string inodeFile,
        string strIno
    ) {
        auto oldDir = buildPath(this.pseudoStorage, attr, oldValue);
        auto newDir = buildPath(this.pseudoStorage, attr, newValue);
        string apcName = buildPath(attr, newValue);
        if (!exists(newDir)) {
            safeMkdir(newDir);
            string stInoFile = buildPath(newDir, "st_ino");
            ino_t attrCategIno = offsetAttrPC + crc32(apcName);
            std.file.write(stInoFile, attrCategIno.to!string);
        }
        string oldInoPath = buildPath(oldDir, strIno);
        if (exists(oldInoPath)) {
            std.file.remove(oldInoPath);
        }
        string newInoPath = buildPath(newDir, strIno);
        if (!exists(newInoPath)) {
            amalthea.fs.hardlink(inodeFile, newInoPath);
        }
    }

    void updateFormatAttrs(string path, ino_t ino) {
        string specDir = getSpecInodeDir(path)[0];
        auto attrs = AttrControl(buildPath(specDir, "attrs"));
        string oldFormat = attrs.get("vitis.apc.format");
        string oldGroup = attrs.get("vitis.apc.group");
        string ext = getExtension(path);
        string dsPath = getRealFilePath(specDir);
        auto ff = amalthea.fileformats.getFileFormat(dsPath, ext);
        if (ff.group.empty) {
            ff.group = "Other";
        }
        if (ff.format == oldFormat && ff.group == oldGroup) {
            return;
        }
        string strIno = to!string(ino);
        updateAttrPseudoCategory(
            "Format", ff.format, oldFormat, dsPath, strIno
        );
        updateAttrPseudoCategory(
            "Group", ff.group, oldGroup, dsPath, strIno
        );
        attrs.set("vitis.apc.format", ff.format);
        attrs.set("vitis.apc.group", ff.group);
    }

    void updateExtensionAttr(
        string oldPath, string newPath, ino_t ino, string specDir
    ) {
        string oldExt = toLower(getExtension(oldPath));
        string newExt = toLower(getExtension(newPath));
        if (oldExt == newExt) return;
        string inodeFile = getRealFilePath(specDir);
        updateAttrPseudoCategory(
            "Extension", newExt, oldExt, inodeFile, to!string(ino)
        );
        auto attrs = AttrControl(buildPath(specDir, "attrs"));
        attrs.set("vitis.apc.extension", newExt);
    }

    void deleteFile(VitisFileInfo info)
    in {
        assert(info.fileType != VFT.pseudocategory);
        assert(info.fileType != VFT.pseudofile);
    } do {
        if (info.fileType != VFT.directory) {
            string attrsFile = buildPath(info.specDir, "attrs");
            deleteInodeFromAttrPseudoCategories(attrsFile, info.ino);
        }
        inoGenerator.freeInodeNumber(info.ino);
    }

    string[] getAllParentAttrFiles(string specDir) {
        string catFile = buildPath(specDir, "categories");
        NameAndIno[] all = DDS.readAll(catFile);
        string[ino_t] aarr;
        foreach(nameAndIno; all) {
            ino_t ino = nameAndIno.ino;
            if (ino !in aarr) {
                aarr[ino] = buildPath(
                    this.inodesStorage, memInoToDirSeq(ino), "attrs"
                );
            }
        }
        return aarr.values;
    }

    struct TimeFlags {
        bool a, m, c;
        bool pa, pm, pc;
    }

    static immutable TimeFlags[string] timeActionTable;

    shared static this() {        // [file ]  [parent]
        timeActionTable = [       // a  m  c  a  m  c
            "chmod":       TimeFlags(0, 0, 1, 0, 0, 0),
            "chown":       TimeFlags(0, 0, 1, 0, 0, 0),
            "link":        TimeFlags(0, 0, 1, 0, 1, 1),
            "read":        TimeFlags(1, 0, 0, 0, 0, 0),
            "opendir":     TimeFlags(1, 0, 0, 0, 0, 0),
            "setxattr":    TimeFlags(0, 0, 1, 0, 0, 0),
            "removexattr": TimeFlags(0, 0, 1, 0, 0, 0),
            "rename":      TimeFlags(0, 0, 1, 0, 1, 1),
            "unlink":      TimeFlags(0, 0, 1, 0, 1, 1),
            "rmdir":       TimeFlags(0, 0, 1, 0, 1, 1),
            "truncate":    TimeFlags(0, 1, 1, 0, 0, 0),
            "write":       TimeFlags(0, 1, 1, 0, 0, 0)
        ];
    }

    void updateTime(string fnName = __FUNCTION__)(VitisFileInfo info) {
        string clearName = fnName.split(".")[$-1];  // without module
        TimeFlags flags = timeActionTable[clearName];
        string attrsFile = buildPath(info.specDir, "attrs");
        timespec ts = getTimeSpec();

        void setTimestamps(ref AttrControl attrs) {
            flags.a && attrs.set("vitis.stat.atime", ts);
            flags.m && attrs.set("vitis.stat.mtime", ts);
            flags.c && attrs.set("vitis.stat.ctime", ts);
        }

        if (flags.a || flags.m || flags.c) {
            auto attrs = AttrControl(attrsFile);
            setTimestamps(attrs);
        }
        if (flags.pa || flags.pm || flags.pc) {
            string[] parentAttrsFiles = getAllParentAttrFiles(info.specDir);
            foreach(parentAttrsFile; parentAttrsFiles) {
                auto attrs = AttrControl(parentAttrsFile);
                setTimestamps(attrs);
            }
        }
    }

    void updateMCTime(VitisFileInfo info, timespec ts) {
        auto attrs = AttrControl(buildPath(info.specDir, "attrs"));
        attrs.set("vitis.stat.mtime", ts);
        attrs.set("vitis.stat.ctime", ts);
    }

    string getDirName(string path) {
        string parentDir = dirName(path);
        Optional!FileFromNameList fromNameList = getFileFromNameList(path);
        if (fromNameList.hasValue()) {
            parentDir = dirName(fromNameList.originalFilePath);
        } else {
            parentDir = dirName(path);
        }
        return parentDir == "." ? "" : parentDir;
    }

}  // class Vitis


struct NameAndIno {
    string name;
    ino_t ino;
}


/// Table of directory descriptors.
class DDTable {
    immutable size_t dirDescrLimit = cast(size_t)int.max + cast(size_t)1;

private:

    string[][int] directoryDescriptors;
    ino_t[int] openDirectories;
    int lastDirDescr = -1;
    Mutex descriptorMutex;

public:

    this() {
        descriptorMutex = new Mutex();
    }

    int makeDirectoryDescriptor(ino_t dirIno, string[] files) {
        descriptorMutex.lock_nothrow();
        scope(exit) descriptorMutex.unlock_nothrow();

        auto descrLen = directoryDescriptors.length;
        fe(descrLen < dirDescrLimit, EMFILE, manyOpenedDirsMsg);
        lastDirDescr++;
        if (lastDirDescr < 0) {
            lastDirDescr = 0;
        }
        while(lastDirDescr in directoryDescriptors) {
            lastDirDescr++;
        }
        directoryDescriptors[lastDirDescr] = files;
        openDirectories[lastDirDescr] = dirIno;
        return lastDirDescr;
    }

    void freeDirectoryDescriptor(int descriptor) {
        descriptorMutex.lock_nothrow();
        scope(exit) descriptorMutex.unlock_nothrow();

        fe(descriptor in directoryDescriptors, EBADF);
        directoryDescriptors.remove(descriptor);
        openDirectories.remove(descriptor);
    }

    string[] takeFiles(int descriptor) {
        descriptorMutex.lock_nothrow();
        scope(exit) descriptorMutex.unlock_nothrow();

        fe(descriptor in directoryDescriptors, EBADF);
        return directoryDescriptors[descriptor];
    }

}  // class DDTable


struct OpenInodeTable {
    void addNewOpenInode(ino_t ino) {
        synchronized {
            if (ino !in openInodeTable) {
                openInodeTable[ino] = OpenInode(1, new Mutex());
            } else {
                openInodeTable[ino].counter++;
            }
        }
    }

    void removeOpenInode(ino_t ino) {
        synchronized {
            if (ino !in openInodeTable) {
                return;
            }
            openInodeTable[ino].counter--;
            if (openInodeTable[ino].counter == 0) {
                openInodeTable.remove(ino);
            }
        }
    }

    void lockInodeMutex(ino_t ino) {
        synchronized {
            if (ino !in openInodeTable) {
                return;
            }
            openInodeTable[ino].mutex.lock();
        }
    }

    void unlockInodeMutex(ino_t ino) {
        synchronized {
            if (ino !in openInodeTable) {
                return;
            }
            openInodeTable[ino].mutex.unlock();
        }
    }


private:

    struct OpenInode {
        size_t counter;
        Mutex mutex;
    }

    OpenInode[ino_t] openInodeTable;

}  // struct OpenInodeTable


string stripNodeId(string name) pure nothrow {  // name without ":iX:" or ":cX:"
    if (name.startsWith(":i") || name.startsWith(":c")) {
        return name.split(":")[2 .. $].join(":");
    }
    return name;
}


// file ID between ":i" or ":c" and ":"
ino_t extractNodeId(string name)
in {
    assert(name.startsWith(":i") || name.startsWith(":c"));
} do {
    string idStr = name[2 .. 1 + name[1 .. $].indexOf(':')];
    ino_t id;
    try {
        id = to!ino_t(idStr);
    } catch (Exception e) {
        fe(EINVAL);
    }
    return id;
}


/// Returns changed file name with added inode number.
string constructRepeatedName(ino_t ino, string fileName) pure nothrow {
    if (fileName.startsWith(":i")) {
        return fileName;
    }
    return ":i" ~ ino.to!string ~ ":" ~ fileName;
}


string constructNameInCategory(ino_t catIno, string fileName) pure nothrow {
    return ":c" ~ catIno.to!string ~ ":" ~ fileName;
}


string constructUnambiguousPath(ino_t ino) pure nothrow {
    return ":i" ~ ino.to!string ~ ":";
}


string getExtension(string filepath) {
    string ext = extension(filepath);
    return ext.empty ? ext : ext[1 .. $];
}


string stripSlashes(string line) nothrow {
    return line.stripLeft('/').stripRight('/');
}


int openFile(string path, int flags) {
    int fd = unix_fcntl.open(path.toStringz, flags);
    fe(fd != -1, errno);
    return fd;
}


class InoGenerator {
    immutable static BitArray zeros;
    // number of hierarchy levels for any inode dir (like "04/F8/17/AE/06")
    immutable static size_t nlevels = 5;

    shared static this() {
        ubyte[] zerosTemplate = new ubyte[256/8];
        // 16 records of 256 bytes
        InoGenerator.zeros = BitArray(zerosTemplate, 256);
    }

    this(string inodesStorage) {
        this.inodesStorage = inodesStorage;
    }

    Tuple!(ino_t, string) generateInodeNumber() {
        ubyte[] seq = new ubyte[nlevels];
        string path = this.inodesStorage;
        size_t indexInSeq;
        while(true) {
            string tablePath = buildPath(path, "inotable");
            if (!exists(tablePath)) {
                BitArray initTableContent = zeros.dup;
                if (indexInSeq == nlevels - 1) {
                    initTableContent[0] = 1;  // 00/00/00/00/00 isn't available
                    string zeroInodePath = buildPath(path, "00");
                    std.file.mkdir(zeroInodePath);
                }
                std.file.write(tablePath, cast(ulong[])initTableContent);
            }
            auto bits = readTable(tablePath);
            size_t freePosNumber = (~bits).bitsSet().walkLength;
            fe(freePosNumber != 0, EDQUOT);
            size_t freePosition = (~bits).bitsSet().takeOne.front;
            path = buildPath(path, format!"%02X"(freePosition));
            if (!exists(path)) {
                collectException(std.file.mkdir(path));
            }
            seq[indexInSeq] = cast(ubyte)freePosition;
            indexInSeq++;
            if (indexInSeq == nlevels) {
                bits[freePosition] = 1;
                std.file.write(tablePath, cast(ulong[])bits);
                if (freePosNumber == 1) {
                    unwind(seq[0 .. $-1], true);
                }
                break;
            }
            continue;
        }
        ino_t ino;
        seq = seq.reverse;  // for correct byte order
        memcpy(&ino, seq.ptr, seq.length);
        return tuple(ino, path);
    }

    void freeInodeNumber(ino_t oldNumber) {
        string oldInoDir = inodesStorage;
        static immutable fmt = "%0" ~ to!string(nlevels * 2) ~ "X";
        string[] inoDirs = divideByElemNumber(format!fmt(oldNumber), 2);
        foreach(dir; inoDirs) {
            oldInoDir = buildPath(oldInoDir, dir);
        }
        string tablePath = buildPath(dirName(oldInoDir), "inotable");
        auto bits = readTable(tablePath);
        size_t bitIndex = oldNumber & 0xFF;  // 0xABCD -> 0x00CD
        bits[bitIndex] = 0;
        std.file.write(tablePath, cast(ulong[])bits);
        std.file.rmdirRecurse(oldInoDir);
        size_t freePosNumber = (~bits).bitsSet().walkLength;
        // if number of free numbers is 1, then there were just none at all
        if (freePosNumber == 1) {
            auto seq = inoDirs.map!(a => parse!ubyte(a, 16)).array;
            unwind(seq[0 .. $-1], false);
        }
    }

    private BitArray readTable(string tablePath) {
        ubyte[] content = cast(ubyte[])std.file.read(tablePath);
        fe(content.length == 32, EIO);  // 256 bits/table, 8 bits/byte
        return BitArray(content, content.length * ubyte.sizeof * 8);
    }

    private void unwind(ubyte[] reducedSeq, bool forGeneration)
    in {
        assert(reducedSeq.length > 0);
    } do {
        string currentTable = inodesStorage;
        foreach(value; reducedSeq[0 .. $-1]) {
            currentTable = buildPath(currentTable, format!"%02X"(value));
        }
        currentTable = buildPath(currentTable, "inotable");
        auto bits = readTable(currentTable);
        size_t freePosNumber = (~bits).bitsSet().walkLength;
        size_t position = reducedSeq[$-1];
        bits[position] = forGeneration ? 1 : 0;
        std.file.write(currentTable, cast(ulong[])bits);
        if (reducedSeq.length > 1) {
            if (freePosNumber == (forGeneration ? 1 : 0)) {
                unwind(reducedSeq[0 .. $-1], forGeneration);
            }
        }
    }

    private string inodesStorage;
}


static struct DDS {
    static immutable size_t initialSize = 4096;
    static immutable size_t nameOffset = 0;
    static immutable size_t inoOffset = 248;
    static immutable size_t rowLength = 256;
    static immutable size_t inoSize = 8;
    static immutable size_t specFieldSize = 3;
    static immutable size_t specFieldPos = inoOffset - specFieldSize;

    static alias Row = ubyte[DDS.rowLength];

    static void initialize(string ddsPath) {
        fe(!exists(ddsPath), EEXIST);
        std.file.write(ddsPath, "");
        DDS.resize(ddsPath, DDS.initialSize);
    }

    private static bool areEqualBytes(ubyte[] arr1, ubyte[] arr2) nothrow {
        if (arr1.length != arr2.length) {
            return false;
        }
        for (ssize_t i = cast(ssize_t)arr1.length - 1; i >= 0; i--) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    static NameAndIno[] readAll(string ddsPath) {
        auto file = File(ddsPath, "r");
        applyFileAdvise(file, FileAdvice.sequential);
        NameAndIno[] list;
        foreach(ubyte[] buffer; file.byChunk(rowLength)) {
            if (buffer[0] == 0) {  // empty record
                break;
            }
            string name = (cast(char*)buffer.ptr).fromStringz.idup;
            fe(name.length <= Vitis.maxNameLength, EIO);
            ino_t ino = numFromPtr!ino_t(buffer.ptr + DDS.inoOffset);
            list ~= NameAndIno(name, ino);
        }
        return list;
    }

    static Optional!(ino_t[]) collectInoExcept(string ddsPath, ino_t excIno) {
        auto file = File(ddsPath, "r");
        applyFileAdvise(file, FileAdvice.sequential);
        Optional!(ino_t[]) res;
        ino_t[] numbers;
        foreach(ubyte[] buffer; file.byChunk(DDS.rowLength)) {
            if (buffer[0] == 0) {  // end of records
                break;
            }
            ino_t ino = numFromPtr!ino_t(buffer.ptr + DDS.inoOffset);
            if (ino == excIno) {
                return res;
            }
            numbers ~= ino;
        }
        res.set(numbers);
        return res;
    }

    static ino_t[] findName(string ddsPath, string name) {
        auto file = File(ddsPath, "r");
        applyFileAdvise(file, FileAdvice.sequential);
        ino_t extractedIno = 0;
        if (name.startsWith(":i")) {
            extractedIno = extractNodeId(name);
            name = stripNodeId(name);
        }
        ubyte[] nameAsBytes = cast(ubyte[])name;
        nameAsBytes ~= 0;
        size_t len = nameAsBytes.length;
        bool nameIsEmpty = len == 1;  // cases with number but without name
        ino_t[] res;
        foreach(ubyte[] buffer; file.byChunk(DDS.rowLength)) {
            if (buffer[0] == 0) {  // end of records
                break;
            }
            if (nameIsEmpty || areEqualBytes(nameAsBytes, buffer[0 .. len])) {
                ino_t ino = numFromPtr!ino_t(buffer.ptr + DDS.inoOffset);
                res ~= ino;
                if (extractedIno == ino) {
                    break;
                }
            }
        }
        return res;
    }

    static void add(string ddsPath, string name, ino_t ino)
    in {
        assert(name.length <= Vitis.maxNameLength);
    } do {
        auto lockableFile = File(ddsPath, "r+");
        lockableFile.lock();
        size_t freePosition = 0;
        uint lastRecordIndex = 0;

        auto firstRecord = lockableFile.rawRead(new ubyte[DDS.rowLength]);
        if (firstRecord[0] != 0) {  // not first record
            memcpy(
                &lastRecordIndex,
                firstRecord.ptr + DDS.specFieldPos,
                DDS.specFieldSize
            );
            version(BigEndian) lastRecordIndex = swapEndian(lastRecordIndex);
            size_t lastIndexByteOffset = lastRecordIndex * DDS.rowLength;
            freePosition = lastIndexByteOffset + DDS.rowLength;
            if (lockableFile.size == freePosition) {  // EoF
                DDS.resize(lockableFile, lockableFile.size * 2);
            }
            lastRecordIndex++;
        }
        // else freePosition is 0 and lastRecordIndex is 0

        ubyte[DDS.rowLength] row;
        memcpy(row.ptr, name.ptr, name.length);
        version(BigEndian) ino = swapEndian(ino);
        memcpy(row.ptr + DDS.inoOffset, &ino, DDS.inoSize);
        lockableFile.seek(freePosition);
        lockableFile.rawWrite(row);

        ubyte[DDS.specFieldSize] specField;
        specField = arrFromNum!specFieldSize(&lastRecordIndex);
        lockableFile.seek(DDS.specFieldPos);
        lockableFile.rawWrite(specField);
    }

    static void remove(string ddsPath, string name, ino_t ino) {
        name = stripNodeId(name);
        auto lockableFile = File(ddsPath, "r+");
        lockableFile.lock();
        Row lastRecord;
        int lastIndex;
        S!(lastRecord, lastIndex) = readLastRecord(lockableFile);
        fe(lastIndex != -1, ENOENT, "DDS is empty");

        int lineIndex;
        Row _;
        S!(_, lineIndex) = findLine(lockableFile, name, ino);
        if (lineIndex == 0 && lastIndex == 0) {  // single record
            lockableFile.seek(0);
            Row emptyRow;
            lockableFile.rawWrite(emptyRow);
            return;
        }
        size_t positionToDelete = lineIndex * DDS.rowLength;
        lockableFile.seek(positionToDelete);
        lockableFile.rawWrite(lastRecord);  // rewrite free record

        size_t lastElementBytePos = lastIndex * DDS.rowLength;
        lockableFile.seek(lastElementBytePos);
        Row emptyRow;
        lockableFile.rawWrite(emptyRow);

        lastIndex--;
        ubyte[DDS.specFieldSize] specField;
        specField = arrFromNum!specFieldSize(&lastIndex);
        lockableFile.seek(DDS.specFieldPos);
        lockableFile.rawWrite(specField);
    }

    static void findAndReplace(
        string ddsPath,
        string oldName, ino_t oldDirIno,
        string newName, ino_t newDirIno
    ) {
        auto lockableFile = File(ddsPath, "r+");
        lockableFile.lock();
        Row row;
        int lineIndex;
        S!(row, lineIndex) = findLine(lockableFile, oldName, oldDirIno);
        size_t positionToReplace = lineIndex * DDS.rowLength;
        memcpy(row.ptr, newName.ptr, newName.length);
        memcpy(row.ptr + DDS.inoOffset, &newDirIno, inoSize);
        lockableFile.seek(positionToReplace);
        lockableFile.rawWrite(row);
    }

    static void resize(string ddsPath, size_t newSize) {
        amalthea.fs.truncate(ddsPath, newSize);
    }
    static void resize(File file, size_t newSize) {
        int ret = unix_unistd.ftruncate(file.fileno, newSize);
        fe(ret != -1, errno);
    }

    static bool isEmpty(string ddsPath) {
        ubyte[] oneByte = cast(ubyte[])std.file.read(ddsPath, 1);
        if (oneByte.length > 0 && oneByte[0] == 0) {
            return true;
        }
        return false;
    }

private:

    pragma(inline)
    static T numFromPtr(T)(ubyte* src, size_t nbytes = T.sizeof) {
        T dst;
        memcpy(&dst, src, nbytes);
        version(BigEndian) dst = swapEndian(dst);
        return dst;
    }

    pragma(inline)
    static auto arrFromNum(size_t nbytes, T)(T* src) {
        version(BigEndian) *src = swapEndian(*src);
        ubyte[nbytes] dst;
        memcpy(dst.ptr, src, nbytes);
        return dst;
    }

    static Tuple!(Row, int) readLastRecord(File file) {
        file.seek(0);
        Row first;
        fread(first.ptr, DDS.rowLength, 1, file.getFP());
        Row last;
        int lastIndex = -1;
        if (first[0] == 0) {
            return tuple(last, lastIndex);
        }
        lastIndex = numFromPtr!int(first.ptr + specFieldPos, specFieldSize);
        size_t byteOffset = lastIndex * DDS.rowLength;
        file.seek(byteOffset);
        fread(last.ptr, DDS.rowLength, 1, file.getFP());
        return tuple(last, lastIndex);
    }

    static Tuple!(Row, int) findLine(File f, string name, ino_t ino) {
        if (name.startsWith(":i")) {
            name = stripNodeId(name);
        }
        ubyte[] nameAsBytes = cast(ubyte[])name;
        nameAsBytes ~= 0;
        size_t len = nameAsBytes.length;
        int lineIndex = -1;
        Row record;
        bool found = false;
        f.seek(0);
        foreach(buffer; f.byChunk(rowLength)) {
            lineIndex++;
            record = buffer.dup;
            if (buffer[0] == 0) {  // end of records
                break;
            }
            if (areEqualBytes(nameAsBytes, buffer[0 .. len])) {
                ino_t foundIno;
                memcpy(&foundIno, buffer.ptr + DDS.inoOffset, DDS.inoSize);
                if (ino == foundIno) {
                    found = true;
                    break;
                }
            }
        }
        fe(found, ENOENT, format!"DDS doesn't contain [%s, %s]"(name, ino));
        return tuple(record, lineIndex);
    }
}


enum VFT {  // VitisFileType
    socket = 's',
    symlink = 'l',
    regularFile = '-',
    blockDevice = 'b',
    directory = 'd',
    charDevice = 'c',
    pipe = 'p',
    pseudocategory = 'a',
    pseudofile = 'n',
    unknown = 'z'
}


enum PseudoCategoryType {
    none,
    attribute,
    expression,
    nameList,
    pathList
}
alias PCT = PseudoCategoryType;


enum PseudoFileType {
    none,
    note,
    imLink,
    pathItem
}


struct VitisFileInfo {
    VFT fileType;
    string specDir;
    ino_t ino;
    PseudoCategoryType pct;
    PseudoFileType pft;

    bool isDir() pure const nothrow {
        return fileType == VFT.directory;
    }
    bool isPseudoCategory() pure const nothrow {
        return fileType == VFT.pseudocategory;
    }
    bool isPseudoFile() pure const nothrow {
        return fileType == VFT.pseudofile;
    }
    bool isPseudo() pure const nothrow {
        return this.isPseudoCategory() || this.isPseudoFile();
    }
}


struct AttrControl {

    static immutable tsAttrs = [
        "vitis.stat.btime",
        "vitis.stat.mtime",
        "vitis.stat.atime",
        "vitis.stat.ctime"
    ];

    string attrFile;

    this(string attrFilePath) {
        this.attrFile = attrFilePath;
    }

    void initContent(string canonName, VFT type, timespec ts, mode_t mode) {
        mode &= antitypeMask;
        immutable rowFmt = `"%s","%s"`;
        ContextMode contextMode = getContextMode();
        contextMode.umask |= getCurrentUmask();
        mode = mode & (~contextMode.umask);
        immutable filemodeFmt = `"%s","%012b"`;

        string content = format!"%s\n%s\n%s\n%s\n%s\n%s\n%s\n"(
            `"key","value"`,
            format!rowFmt("vitis.canon_name", canonName),
            format!rowFmt("vitis.filetype", cast(char)type),
            format!filemodeFmt("vitis.filemode", mode),
            format!rowFmt("vitis.userid", contextMode.uid),
            format!rowFmt("vitis.groupid", contextMode.gid),
            format!rowFmt("vitis.stat.nlink", "2")
        );

        foreach(attr; tsAttrs) {
            content ~= format!`"%s","%s|%s"`(attr, ts.tv_sec, ts.tv_nsec);
            content ~= "\n";
        }
        std.file.write(this.attrFile, content);
    }

    void set(string name, string newValue) {
        synchronized {
            auto csv = CSV(this.attrFile);
            string currentValue;
            try {
                currentValue = csv.getValueOfField("key", name, "value");
                csv.setValueOfField("key", name, "value", newValue);
            } catch (CsvException e) {
                csv.addRow(name, newValue);
            }
            csv.rewrite();
        }
    }

    void set(string name, timespec ts)
    in {
        assert(name.isAmong(tsAttrs));
    } do {
        string newValue = format!`%s|%s`(ts.tv_sec, ts.tv_nsec);
        set(name, newValue);
    }

    string get(string name, string defaultValue = "") const {
        auto csv = CSV(this.attrFile);
        try {
            return csv.getValueOfField("key", name, "value");
        } catch (CsvException e) {
            return defaultValue;
        }
    }

    Optional!string optGet(string name) const {
        auto csv = CSV(this.attrFile);
        try {
            return optional(csv.getValueOfField("key", name, "value"));
        } catch (CsvException e) {
            return optional!string;
        }
    }

    mode_t readPerm() const {
        auto csv = CSV(this.attrFile);
        auto strBits = csv.getValueOfField("key", "vitis.filemode", "value");
        return parse!mode_t(strBits, 2);
    }

    FileRights readFileRights() const {
        FileRights fr;
        auto t = CSV(this.attrFile);
        auto strBits = t.getValueOfField("key", "vitis.filemode", "value");
        fr.permissions = parse!mode_t(strBits, 2);
        fr.owner = t.getValueOfField("key", "vitis.userid", "value").to!uid_t;
        fr.group = t.getValueOfField("key", "vitis.groupid", "value").to!gid_t;
        return fr;
    }

    string[] getAllKeys() const {
        auto csv = CSV(this.attrFile);
        string[][] table = csv.getTable();
        if (table.length == 0) {
            return [];
        }
        table = table[1 .. $];  // without header
        string[] res;
        for (size_t rowIndex = 0; rowIndex < table.length; rowIndex++) {
            res ~= table[rowIndex][0];
        }
        return res;
    }

    void remove(string name) {
        auto csv = CSV(this.attrFile);
        csv.deleteRowByKeyAndValue("key", name);
        csv.rewrite();
    }
}  // struct AttrControl


uint crc32(string message) {
	auto crc = new std.digest.crc.CRC32Digest();
    ubyte[] h = crc.digest(message);
    assert(h.length == 4);
    uint res = (h[3] << 24) + (h[2] << 16) + (h[1] << 8) + h[0];
    return res;
}


string cutOffSpecEnding(string path) {
    fe(path.length < ssize_t.max, ENAMETOOLONG);
    ssize_t lfIndex = cast(ssize_t)path.length - 1;
    for (; lfIndex > -1; lfIndex--) {
        if (path[lfIndex] == '\\') {
            break;
        }
    }
    if (lfIndex != -1) {
        return path[0 .. lfIndex];
    }
    return path;
}



string convInoToDirSeq(ino_t ino) pure nothrow {
    try {
        immutable fmt = "%0" ~ to!string(InoGenerator.nlevels * 2) ~ "X";
        return divideByElemNumber(format!fmt(ino), 2).join("/");
    } catch(Exception e) {
        return "";
    }
}


alias memInoToDirSeq = memoize!(convInoToDirSeq, 100_000);


struct ContextMode {
    uid_t uid;
    gid_t gid;
    mode_t umask;
}


ContextMode getContextMode() {
    fuse_context* context = fuse_get_context();
    return ContextMode(context.uid, context.gid, context.umask);
}


struct FileRights {
    mode_t permissions;
    uid_t owner;
    gid_t group;
}


timespec getTimeSpec() {
    timespec currTime;
    int ret = clock_gettime(0, &currTime);
    fe(ret == 0, errno);
    return currTime;
}


bool doesExceptionCatch(T : Throwable = Exception, E)(lazy E expr, ref E res) {
    try {
        res = expr();
    } catch (T e) {
        return true;
    }
    return false;
}
bool doesExceptionCatch(T = Exception, E)(lazy E expression) {
    try {
        expression();
    } catch (T e) {
        return true;
    }
    return false;
}


void safeMkdir(string path) {
    errno = 0;
    int ret = unix_stat.mkdir(path.toStringz, oct!777);
    if (ret == -1) {
        enforce(errno == EEXIST, new std.file.FileException("safeMkdir"));
    }
}


struct Optional(T) {
    private bool state = false;
    private T base;

    bool hasValue() nothrow const {
        return this.state;
    }

    T get(T defaultValue = T.init) nothrow {
        return this.hasValue() ? this.base : defaultValue;
    }

    void set(T value) {
        this.base = value;
        this.state = true;
    }

    string toString() const {
        if (!this.hasValue) {
            return "Optional(false)";
        }
        return format!"Optional(true, %s)"(base);
    }

    alias base this;
}


Optional!T optional(T)(T value) {
    Optional!T obj;
    obj.set(value);
    return obj;
}


Optional!T optional(T)() {
    Optional!T obj;
    return obj;
}
