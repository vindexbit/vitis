/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2018, 2021-2022, 2024-2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module rpn;  // reverse polish notice


import std.algorithm : among, filter;
import std.array : array, empty, split;
import std.exception : enforce;
import std.file : exists;

import amalthea.libcore : RealizeException;
import amalthea.dataprocessing :
    isAmong, mixAssociativeArrays, calcUnion, calcIntersection, calcComplement;

alias ssize_t = ptrdiff_t;

immutable wrongMsg = "Wrong expression.";

immutable operands = ["{", "}", "@", "%", "-"];


enum ElementType {
    NONE,
    LBRACE, RBRACE,
    CATEGORY,
    UNION, INTERSECTION, COMPLEMENT
}
alias ET = ElementType;


@property ET etype(string line) {
    switch (line) {
        case "{": return ET.LBRACE;
        case "}": return ET.RBRACE;
        case "@": return ET.UNION;
        case "%": return ET.INTERSECTION;
        case "-": return ET.COMPLEMENT;
        default:  return ET.CATEGORY;
    }
}

@property bool isMathSign(ET value) {
    return cast(bool) value.among(ET.UNION, ET.INTERSECTION, ET.COMPLEMENT);
}

@property bool isBrace(ET value) {
    return cast(bool) value.among(ET.LBRACE, ET.RBRACE);
}

@property int prior(string line) {
    if (line.etype.among(ET.LBRACE, ET.RBRACE))    return 1;
    if (line.etype == ET.INTERSECTION)             return 2;
    if (line.etype.among(ET.UNION, ET.COMPLEMENT)) return 3;
    return int.max;
}


/// The function counts the number of opening and closing brackets
void validateBrackets(
    string[] expression, string lbracket = "{", string rbracket = "}"
) {
    size_t lbrackets, rbrackets;
    immutable msg = "Incorrect use of brackets.";
    foreach (element; expression) {
        lbrackets += (element == lbracket);
        rbrackets += (element == rbracket);
        ve(rbrackets <= lbrackets, msg);
    }
    ve(lbrackets == rbrackets, msg);
}


void validateExpression(string rawExpression) {
    string[] elements = rawExpression.split('\n');
    elements = elements[0 .. $-1];
    validateExpression(elements);
}


void validateExpression(string[] expression) {
    ve(!expression.empty || expression.length < 3, "No expression.");

    validateBrackets(expression);

    ET[] elementTypeArray;
    elementTypeArray.length = expression.length;
    foreach(i, ref el; elementTypeArray) {
        el = expression[i].etype;
    }
    if (isMathSign(elementTypeArray[0])) {
        throw new ValidException("Expression cannot begin with such sign.");
    }
    ve(elementTypeArray[0].isAmong(ET.LBRACE, ET.CATEGORY));
    ve(elementTypeArray[$-1].isAmong(ET.RBRACE, ET.CATEGORY));
    foreach(i, el; elementTypeArray) {
        ET nextEl;
        if (elementTypeArray.length > i+1) {
            nextEl = elementTypeArray[i+1];
        }
        if (el == ET.LBRACE) {
            ve(nextEl.isAmong(ET.CATEGORY, ET.RBRACE));
        } else if (el == ET.CATEGORY) {
            ve(nextEl == ET.NONE || nextEl.isMathSign || nextEl == ET.RBRACE);
        } else if (el.isMathSign) {
            ve(nextEl.isAmong(ET.CATEGORY, ET.RBRACE));
        } else if (el == ET.RBRACE) {
            ve(nextEl == ET.NONE || nextEl.isMathSign || nextEl == ET.RBRACE);
        }
    }
}


string[] extractCategoriesOnly(string[] infixNotation) {
    return infixNotation.filter!(el => el.etype == ET.CATEGORY).array;
}


string[] toReversePolishNotation(string[] infixNotation) {
    validateExpression(infixNotation);
    string[] operationStack;
    string[] exitArray;
    foreach(el; infixNotation) {
        if (el.etype == ET.CATEGORY) {
            exitArray ~= el;
        } else if (el.etype.isMathSign || el.etype.isBrace) {
            if (operationStack.empty) {
                operationStack ~= el;
            } else {
                if ((operationStack[$-1].prior <= el.prior)
                    && !el.etype.isBrace
                    && !operationStack[$-1].etype.isBrace
                   ) {
                    exitArray ~= operationStack[$-1];
                    operationStack[$-1] = el;
                } else if (ET.RBRACE == el.etype) {
                    auto lastindex = cast(ssize_t)(operationStack.length) - 1;
                    for (ssize_t i = lastindex; i >= 0; i--) {
                        if (operationStack[i].etype != ET.LBRACE) {
                            exitArray ~= operationStack[i];
                            operationStack.length--;
                        } else {
                            operationStack.length--;
                            break;
                        }
                    }
                } else {
                    operationStack ~= el;
                }
            }
        }
    }
    for(int i = cast(int)operationStack.length-1; i >= 0; i--) {
        exitArray ~= operationStack[i];
    }
    operationStack.length = 0;
    return exitArray;
}


/// Exception related to validation.
class ValidException : Exception { mixin RealizeException; }


pragma(inline, true)
void ve(T)(T value, string message = wrongMsg) {
    if (!value) {
        throw new ValidException(message);
    }
}
