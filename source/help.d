/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2021-2022, 2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

import std.array : empty;
import std.exception : enforce;
import std.file : exists, FileException, readText;
import std.path : buildPath;
import std.stdio : writeln;
import std.string : strip;

static import amalthea.langlocal;
import amalthea.langlocal : _s;
static import amalthea.sys;


private string getHelpDir() {
    string lang = amalthea.langlocal.getSystemLanguage();
    string baseHelpDir = amalthea.sys.getAppHelpDir();
    string helpDir = buildPath(baseHelpDir, lang, "vitis");
    if (!exists(helpDir)) {
        helpDir = buildPath(baseHelpDir, "en_US", "vitis");
    }
    enforce(helpDir.exists, new FileException("Help file does not exist."._s));
    return helpDir;
}


/// Prints help information.
void showHelp(string command = "") {
    string dir = getHelpDir();
    string name = command.empty ? "help_begin" : "help_" ~ command;
    string txtFilePath = buildPath(dir, name ~ ".txt");
    writeln(txtFilePath.readText.strip);
}
