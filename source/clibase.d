/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

import std.algorithm, std.array, std.stdio, std.string;

import amalthea.langlocal;
import amalthea.libcore : RealizeException;
import amalthea.terminal : FGColor, cwrite, isTTY;
import amalthea.dataprocessing :
    extractOptionValue, extractOptionRange, stripLeft, isAmong;

enum SUCCESS = 0;
enum COMMON_ERROR = 1;
enum WRONG_USAGE = 2;


class ConfException : Exception { mixin RealizeException; }


enum OutputTextType {
    simple,
    warning,
    error,
    category,
    filename,
    propertyName,
    propertyValue
}
alias TT = OutputTextType;


void print(alias tt = TT.simple, S...)(S args) {
    if (!stdout.isTTY && tt != TT.warning && tt != TT.error) {
        stdout.write(args);
        return;
    } else if (!stderr.isTTY && tt.isAmong(TT.warning, TT.error)) {
        stderr.write(args);
        return;
    }
    final switch(tt) {
        case TT.simple:        std.stdio.write(args);            break;
        case TT.warning:       cwrite(FGColor.yellow, args);     break;
        case TT.error:         cwrite(FGColor.light_red, args);  break;
        case TT.category:      cwrite(FGColor.light_blue, args); break;
        case TT.filename:      cwrite(FGColor.light_cyan, args); break;
        case TT.propertyName:  cwrite(FGColor.light_blue, args); break;
        case TT.propertyValue: cwrite(FGColor.green, args);      break;
    }
}
deprecated alias tprint = print;


void printLine(alias tt=TT.simple, S...)(S args) {
    print!tt(args, "\n");
}
deprecated alias tprintln = printLine;


bool isOption(string parameter) {
    return parameter.startsWith("-");
}


/*******************************************************************************
 * Interactive element of UI: getting an answer (yes/no) to a question.
 */
bool requestResponse(string question = "") {
    print!(TT.simple)(question);
    string input = readln.strip.toLower;
    return input.isAmong("y"._s, "yes"._s, "y", "yes");
}


/// Exception related to validation.
class ValidException : Exception { mixin RealizeException; }


/// Exception related to wrong usage of commands.
class WrongUsage : Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }

    this(
        const string[] messages,
        string file = __FILE__,
        size_t line = __LINE__
    ) {
        auto msg = std.array.join(messages, "\n");
        super(msg, file, line);
    }
}


/// Other errors.
class CommonError : Exception { mixin RealizeException; }

