/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.cmd_assign;

import core.sys.posix.sys.stat : ino_t;

import std.algorithm : endsWith;
static import std.array;
import std.exception : enforce;
static import std.file;
import std.file : FileException;
import std.getopt;
import std.path : baseName, dirName, buildPath;
import std.range : empty;
import std.stdio : readln;
import std.string : indexOf;

static import amalthea.fs;
import amalthea.fs : exists, getFiles, xattr, FileEntry;
import amalthea.dataprocessing :
    extractOptionValue,
    extractOptionRange,
    stripRight,
    stripLeft,
    isAmong;
import amalthea.terminal : FGColor, cwrite, isTTY;
import amalthea.langlocal : _s;

import clibase : print, printLine, requestResponse, TT, CommonError, WrongUsage;
import configuration : getMountPoint;
static import filesystem;
import help : showHelp;

import cmd.common;

struct AssignOptions {
    bool yesMode;
    bool all;
    bool allowSameNames;
    bool createCategories;
    bool updateCanonName;
}


immutable wrongMsg = "Incorrect use of 'vitis assign'.";
immutable seeHelpMsg = "See: vitis assign --help";
immutable string[] errMessages;
shared static this() {
    errMessages = [wrongMsg._s, seeHelpMsg._s];
}

enum doYouWantToUseThisName = "Do you want to use this repetitive name? >: ";
enum doYouWantToSkip = "Do you want to skip it? >: ";


void runAssignCommand(string[] args) {
    if (args[2].isAmong("--help", "-h")) {
        enforce(args.length == 3, new WrongUsage(errMessages));
        showHelp("assign");
        return;
    }
    std.file.chdir(getMountPoint());

    AssignOptions options;
    args.getopt(
        config.passThrough,
        "all", &options.all,
        "yes", &options.yesMode,
        "allow-same-names|S", &options.allowSameNames,
        "create-categories|M", &options.createCategories,
        "update-canon-name|U", &options.updateCanonName
    );

    string[] categories = extractOptionRange(args, "-c");
    string[] files = extractOptionRange(args, "-f");
    string[] vitisFiles = extractOptionRange(args, "-v");
    string[] expression = extractOptionRange(args, "-e");
    string newName = extractOptionValue(args, "-n");

    if (!categories.empty) {
        // vitis assign -f <file> ... -c <category> ...
        if (!files.empty) {
            enforce(expression.empty, new WrongUsage(errMessages));
            enforce(vitisFiles.empty, new WrongUsage(errMessages));
            addFilesToVitis(files, categories, options);
            return;
        }
        // vitis assign -v <category/file> ... -c <new_category> ...
        if (!vitisFiles.empty) {
            enforce(expression.empty, new WrongUsage(errMessages));
            foreach (ref f; vitisFiles) {
                f = f.stripLeft('/');
                f = buildPath(getMountPoint(), f);
            }
            addFilesToVitis(vitisFiles, categories, options);
            return;
        }
        // vitis assign -e <expression> -c <category> ...
        if (!expression.empty) {
            addFilesByExpression(expression, categories, options);
        }
    }
    // vitis assign -v <file> -n <new_name>
    if (!newName.empty && !vitisFiles.empty) {
        enforce(vitisFiles.length != 1, new WrongUsage(seeHelpMsg._s));
        assignNewName(vitisFiles[0], newName, options);
    }
}


/*******************************************************************************
 * The function assigns categories to files.
 * This is used for 'vitis assign -c <category> ... -f <file>...'.
 */
void addFilesToVitis(
    string[] files, string[] categories, AssignOptions options
) {
    bool doCreateCategories = options.yesMode || options.createCategories;
    try {
        categories = prepareCategories(categories, doCreateCategories);
    } catch (WrongUsage wu) {
        throw new WrongUsage([wu.msg] ~ errMessages);
    }
    files = filterFiles(files);
    foreach(cat; categories) {
        foreach(ref f; files) {
            string path = buildPath(cat, baseName(f));  // new path
            if (options.allowSameNames && !path.endsWith(repNameEnding)) {
                path ~= repNameEnding;
            }
            try {
                while (!path.endsWith(repNameEnding) && exists(path)) {
                    printLine!W(path, ": this name exists."._s);
                    bool ans = requestResponse(doYouWantToUseThisName._s);
                    if (ans) {
                        path ~= repNameEnding;
                    } else {
                        ans = requestResponse(doYouWantToSkip._s);
                        if (!ans) {
                            print!(TT.simple)("Input name: "._s);
                            print!(TT.simple)(dirName(path) ~ "/");
                            string input = readln;
                            path = buildPath(dirName(path), input);
                        }
                    }
                }
                if (getDeviceOfFile(f) != getVitisDevice()) {
                    amalthea.fs.cp(f, path);
                } else {
                    amalthea.fs.hardlink(f, path);
                }
            } catch(Exception e) {
                printLine!E(e.msg);
                printLine!E("    ", "Source:      "._s, f);
                printLine!E("    ", "Destination: "._s, path);
            }
        }
    }
}


/*******************************************************************************
 * The function assigns categories to files by category set expression.
 * It corresponds to 'vitis assign -c <category> ... -e <expression>'.
 */
void addFilesByExpression(
    string[] expression, string[] categories, AssignOptions options
) {
    string expForFS = std.array.join(expression, "\\") ~ filesystem.expEnding;
    bool fileExists = false;
    try {
        fileExists = std.file.exists(expForFS);
    } catch(FileException e) {
        throw new CommonError(e.msg);
    }
    string errMsg = "The expression contains non-existent categories."._s;
    enforce!CommonError(fileExists, errMsg);

    bool doCreateCategories = options.yesMode || options.createCategories;
    categories = prepareCategories(categories, doCreateCategories);

    foreach (f; getFiles(expForFS)) {
        string fileName = baseName(f.path);
        foreach (cat; categories) {
            string newPath = buildPath(cat, fileName);
            if (options.allowSameNames && !newPath.endsWith(repNameEnding)) {
                newPath ~= repNameEnding;
            }
            amalthea.fs.hardlink(f.path, buildPath(cat, fileName));
        }
    }
}


string[] filterFiles(string[] files) {
    string[] finalFiles;
    foreach(f; files) {
        if (!amalthea.fs.exists(f)) {
            printLine!W(f, ": file not found."._s, " ", "Skipped."._s);
            continue;
        }
        finalFiles ~= f;
    }
    return finalFiles;
}


/*******************************************************************************
 * The function assigns new names.
 * This is used for 'vitis assign -v <file> -n <new_name>'.
 */
void assignNewName(string oldPath, string newName, AssignOptions options) {
    newName = newName.stripLeft('/').stripRight('/');
    enforce(-1 == newName.indexOf('/'), new WrongUsage(errMessages));
    string mp = getMountPoint();
    if (options.updateCanonName) {
        xattr.set(oldPath, "vitis.canon_name", newName);
    }
    if (options.all) {
        FileEntry[] nameList;
        nameList = getFiles(oldPath ~ filesystem.fileNameListEnding);
        foreach(item; nameList) {
            string itemName = baseName(item.path);
            ino_t catId = filesystem.extractNodeId(itemName);
            string cat = filesystem.constructUnambiguousPath(catId);
            string newPath = buildPath(mp, cat, newName);
            if (options.allowSameNames && !newPath.endsWith(repNameEnding)) {
                newPath ~= repNameEnding;
            }
            try {
                std.file.rename(item.path, newPath);
            } catch (FileException e) {
                printLine!E(newPath, ": ", e.msg, "Skipped."._s);
            }
        }
    } else {
        string newPath = buildPath(mp, dirName(oldPath), newName);
        if (options.allowSameNames && !newPath.endsWith(repNameEnding)) {
            newPath ~= repNameEnding;
        }
        std.file.rename(oldPath, newPath);
    }
}
