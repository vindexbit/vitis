/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.cmd_delete;

import std.array : empty;
import std.exception : enforce;
import std.getopt;
static import std.file;

import amalthea.dataprocessing : extractOptionRange, isAmong;
import amalthea.langlocal : _s;
static import amalthea.fs;
import amalthea.fs : FileEntry, getFiles;

import clibase : printLine, requestResponse, TT, CommonError, WrongUsage;
import configuration : getMountPoint;
static import filesystem;
import help : showHelp;

immutable wrongMsg = "Incorrect use of 'vitis delete'.";
immutable seeHelpMsg = "See: vitis delete --help";
immutable string[] errMessages;
shared static this() {
    errMessages = [wrongMsg._s, seeHelpMsg._s];
}

import cmd.common;


void runDeleteCommand(string[] args) {
    if (args[2].isAmong("--help", "-h")) {
        enforce(args.length == 3, new WrongUsage(errMessages));
        showHelp("delete");
        return;
    }
    std.file.chdir(getMountPoint());

    bool flagToRemoveAllLinks = false;
    args.getopt(
        config.passThrough,
        "all-links|A", &flagToRemoveAllLinks
    );
    string[] categories = extractOptionRange(args, "-c");
    string[] files = extractOptionRange(args, "-v");
    enforce(categories.empty ^ files.empty, new WrongUsage(errMessages));
    if (!categories.empty) {
        removeCategories(categories, flagToRemoveAllLinks);
    } else if (!files.empty) {
        removeLinks(files, flagToRemoveAllLinks);
    }
}


void removeLinks(string[] vitisFiles, bool flagToRemoveAllLinks) {
    foreach(f; vitisFiles) {
        auto p = getVitisFileFullPath(f);
        if (!amalthea.fs.exists(p)) {
            printLine!W(f, ": file not found."._s, " ", "Skipped"._s);
            continue;
        } else if (amalthea.fs.isDir(p)) {
            printLine!W(f, ": file is directory."._s, " ", "Skipped"._s);
            continue;
        }
        try {
            if (!flagToRemoveAllLinks) {
                std.file.remove(p);
            } else {
                p ~= filesystem.fileNameListEnding;
                FileEntry[] nameList = getFiles(p);
                foreach(item; nameList) {
                    std.file.remove(item.path);
                }
            }
        } catch(Exception e) {
            printLine!E(p, ": failed."._s, e.msg);
        }
    }
}


void removeCategories(string[] categories, bool flagToRemoveAllLinks) {
    foreach(c; categories) {
        auto p = getVitisFileFullPath(c);
        if (!amalthea.fs.exists(p)) {
            printLine!W(c, ": category not found."._s, " ", "Skipped"._s);
            continue;
        } else if (!amalthea.fs.isDir(p)) {
            printLine!W(c, ": file is not category."._s, " ", "Skipped"._s);
            continue;
        }
        try {
            if (!flagToRemoveAllLinks) {
                std.file.rmdirRecurse(p);
            } else {
                p ~= filesystem.fileNameListEnding;
                FileEntry[] nameList = getFiles(p);
                foreach(item; nameList) {
                    std.file.rmdirRecurse(item.path);
                }
            }
        } catch(Exception e) {
            printLine!E(p, ": failed."._s, e.msg);
        }
    }
}
