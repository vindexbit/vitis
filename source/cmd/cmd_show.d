/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.cmd_show;

import core.sys.posix.sys.stat : ino_t;

import std.algorithm;
static import std.array;
import std.format : format;
import std.getopt;
import std.path : baseName, buildPath;
import std.process : spawnProcess;
import std.range : empty;
import std.string : replace, split, rightJustify;

import amalthea.fs;
import amalthea.dataprocessing :
    extractOptionValue,
    extractOptionRange,
    stripLeft,
    isAmong;
import amalthea.terminal : FGColor, cwrite, isTTY;
import amalthea.langlocal;
import amalthea.time;

import configuration : getMountPoint;
static import filesystem;
import clibase;
import help : showHelp;


struct ShowOptions {
    bool categories;
    bool details;
    string numbers;
    SortEnum sortType;
    bool openingMode;
    bool runningMode;
}


enum SortEnum : string {
    name = "name",
    none = "none",
    size = "size",
    time = "time",
    atime = "atime",
    extension = "extension"
}


immutable wrongUsageMsg = "Incorrect use of 'vitis show'.";


void runShowCommand(string[] args) {
    if (args[2].isAmong("--help", "-h")) {
        enforce(args.length == 3, new WrongUsage(wrongUsageMsg._s));
        showHelp("show");
        return;
    }
    std.file.chdir(getMountPoint());
    if (!isOption(args[2])) {
        args = args[0 .. 2] ~ "-e" ~ args[2 .. $];
    }

    ShowOptions options;
    args.getopt(
        config.passThrough,
        "categories", &options.categories,
        "details",    &options.details,
        "n",          &options.numbers,
        "sort",       &options.sortType,
        "open",       &options.openingMode,
        "run",        &options.runningMode
    );

    switch(args[2]) {
        case "-c":
            string dir = extractOptionValue(args, "-c");
            showDirectoryContent(dir, options);
            break;
        case "-e":
            string[] expression = extractOptionRange(args, "-e");
            showFilesByExpression(expression, options);
            break;
        default: throw new WrongUsage([wrongUsageMsg._s]);
    }
}


void showDirectoryContent(string dir, ShowOptions options) {
    dir = dir.stripLeft('/');
    string dirLocation = buildPath(getMountPoint(), dir);
    if (!std.file.exists(dirLocation)) {
        throw new CommonError(dir ~ ": this category doesn't exist."._s);
    } else if (!std.file.isDir(dirLocation)) {
        throw new CommonError(dir ~ ": this file is not category."._s);
    }
    auto files = getFiles(dirLocation);
    if (!options.numbers.empty) {
        files = getRarefiedArrayByNumbers(files, options.numbers);
    }
    size_t i = 1;
    foreach (f; files) {
        if (f.path.empty) {
            continue;
        }
        string name = !isDir(f) ? baseName(f.path) : baseName(f.path) ~ "/";
        showLine(name, i, files.length);
        if (options.categories) {
            printCategories(f.fileno);
        }
        if (options.details) {
            printDetails(f);
        }
        i++;
        if (options.openingMode) {
            spawnProcess(["ufo", f.path]);
        } else if (options.runningMode) {
            spawnProcess([f.path]);
        }
    }
}


private void showLine(
    string file,
    ssize_t currNumber,
    ssize_t maxNumber,
    bool disableNumbers = false
) {
    auto numberOfDigits = calcNumberOfDigits(maxNumber);
    if (!disableNumbers) {
        auto printableNumber = "[" ~ currNumber.to!string ~ "]";
        auto numberField = rightJustify(printableNumber, numberOfDigits+2);
        print(numberField, " ");
    }
    printLine!(TT.filename)(file);
}


private size_t calcNumberOfDigits(ulong x) {
    enum maxDigitsN = ulong.max.to!string.length;
    long p = 10;
    for (size_t i = 1; i < maxDigitsN; ++i) {
        if (x < p) {
            return i;
        }
        p *= 10;
    }
    return maxDigitsN;
}


void showFilesByExpression(string[] expression, ShowOptions options) {
    string expForFS = std.array.join(expression, "\n") ~ "\nxi";
    string errMsg = "The expression contains non-existent categories."._s;
    enforce!CommonError(std.file.exists(expForFS), errMsg);
    auto files = getFiles(expForFS);
    size_t i = 1;
    foreach (f; files) {
        auto name = filesystem.stripNodeId(baseName(f.path));
        showLine(name, i, files.length);
        if (options.categories) {
            printCategories(f.fileno);
        }
        if (options.details) {
            printDetails(f);
        }
        i++;
    }
}


private void printCategories(ino_t fileId) {
    string fileNameByIno = filesystem.constructUnambiguousPath(fileId);
    FileEntry[] nameList;
    fileNameByIno ~= filesystem.fileNameListEnding;
    nameList = getFiles(buildPath(getMountPoint(), fileNameByIno));
    foreach(item; nameList) {
        ino_t catIno = filesystem.extractNodeId(baseName(item.path));
        string catNameByIno = filesystem.constructUnambiguousPath(catIno);
        string[] paths = getAllPaths(buildPath(getMountPoint(), catNameByIno));
        foreach(p; paths) {
            p = p.replace(`"`, `\"`);
            string line = format!"C%u: \"%s\""(catIno, p);
            printLine!(TT.category)(line);
        }
    }
}


private string[] getAllPaths(string file) {
    FileEntry[] symlinks = getFiles(file ~ filesystem.filePathListEnding);
    string[] paths = new string[symlinks.length];
    foreach(i, s; symlinks) {
        paths[i] = std.file.readLink(s.path);
    }
    return paths;
}


string framing(string s, string frame=`"`) {
    return frame ~ s.replace(frame, "\\"~frame) ~ frame;
}


string extractInoNumber(string fileNameWithId) {
    return fileNameWithId.split(":")[1][2 .. $];
}


private void printDetails(FileEntry entry) {
    FileStat fstat = entry.getFileStat();
    string lastModification = amalthea.time.getTimeString(fstat.mtime);
    string lastAccess = amalthea.time.getTimeString(fstat.atime);
    string permissions = amalthea.fs.makeUnixFileModeLine(fstat.mode);
    string normSize = makeHumanOrientedByteSize(fstat.size);
    if (fstat.size >= 1024) {
        normSize = format!"%s (%s %s)"(normSize, fstat.size, "B"._s);
    }

    print!(TT.propertyName)("Modified: "._s);
    print!(TT.propertyValue)(lastModification, "\t");
    print!(TT.propertyName)("Permissions: "._s);
    printLine!(TT.propertyValue)(permissions[1 .. $]);

    print!(TT.propertyName)("Accessed: "._s);
    print!(TT.propertyValue)(lastAccess, "\t");
    print!(TT.propertyName)("Size: "._s);
    printLine!(TT.propertyValue)(normSize);
}


private long getExponent(ulong x) {
    if (x == 0) {
        return 0;
    }
    long exp = -1;
    while(x != 0) {
        x = x >> 10;
        ++exp;
    }
    return exp;
}


private string makeHumanOrientedByteSize(ulong bytes) {
    auto exponent = getExponent(bytes);
    if (exponent > 5) exponent = 5;
    string unit = exponent.predSwitch(
        0, "B"._s,
        1, "KiB"._s,
        2, "MiB"._s,
        3, "GiB"._s,
        4, "TiB"._s,
        5, "PiB"._s,
        null
    );
    real size = to!real(bytes) / (1024^^exponent);
    auto strSize = exponent == 0 ? to!string(size) : format!"%.2f"(size);
    return strSize ~ " " ~ unit;
}
unittest {
    assert(makeHumanOrientedByteSize(0) == "0 B");
    assert(makeHumanOrientedByteSize(1023) == "1023 B");
    assert(makeHumanOrientedByteSize(1024) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1025) == "1.00 KiB");
    assert(makeHumanOrientedByteSize(1024 * 1024 * 512) == "512.00 MiB");
    assert(makeHumanOrientedByteSize(1024L^^3 * 5 / 4) == "1.25 GiB");
    assert(makeHumanOrientedByteSize(1024L^^3 * 7 / 4) == "1.75 GiB");
}


/*******************************************************************************
 * Function returns chopped (rarefied) array
 * by numbers like "1-4,6,8-11,9" or simple number
 */
T[] getRarefiedArrayByNumbers(T)(T[] arr, string numbers) {
    import std.regex;
    T[] resultArray;
    resultArray.length = arr.length;
    if (stringIsUint(numbers)) {
        ssize_t n = to!ssize_t(numbers)-1;
        if (n >= 0 && n < resultArray.length) resultArray[n] = arr[n];
        return resultArray;
    }

    //for valid expression, for example: [1-5,7,12,14-19]
    auto r = ctRegex!(r"^\[[0-9]+(-[0-9]+)?(,+[0-9]+(-[0-9]+)?)*\]$");
    numbers = replace(numbers, ",", ",,");
    if (!numbers.startsWith("[") && !numbers.endsWith("]")) {
        numbers = "[" ~ numbers ~ "]"; //legacy
    }
    auto c = numbers.matchFirst(r);
    if (0 != c.length) {
        immutable
            strRegExpForRanges = `((?P<index1>[0-9]+)-(?P<index2>[0-9]+))`,
            strRegExpForSimpleNumber = `([,\[](?P<index>[0-9]+)[,\]])`;
        auto sharedRegExp
            = ctRegex!(strRegExpForRanges ~ "|" ~ strRegExpForSimpleNumber);

        foreach (el; matchAll(c[0], sharedRegExp)) {
            try {
                for (auto i = el["index1"].to!size_t-1;
                     i < el["index2"].to!size_t;
                     ++i) {
                    if (i >= 0 && i < resultArray.length)
                        resultArray[i] = arr[i];
                }
            }
            //if el["index1"] is invalid
            catch(std.conv.ConvException e) {
                auto index = el["index"].to!size_t-1;
                resultArray[index] = arr[index];
            }
        }
    } else {
        throw new ValidException("Incorrect use of option '-n'."._s);
    }
    return resultArray;
}


bool stringIsUint(string value) {
    import std.uni : isNumber;
    foreach(c; value) if (!(isNumber(to!dchar(c)))) return false;
    return true;
}
