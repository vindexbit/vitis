/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.cmd_create;

import std.array : empty;
import std.exception : enforce;
import std.getopt;
static import std.file;

import amalthea.dataprocessing : extractOptionRange, isAmong;
import amalthea.langlocal : _s;
static import amalthea.fs;

import clibase : printLine, requestResponse, TT, CommonError, WrongUsage;
import configuration : getMountPoint;
static import filesystem;
import help : showHelp;

immutable wrongMsg = "Incorrect use of 'vitis create'.";
immutable seeHelpMsg = "See: vitis create --help";
immutable string[] errMessages;
shared static this() {
    errMessages = [wrongMsg._s, seeHelpMsg._s];
}

import cmd.common;


void runCreateCommand(string[] args) {
    if (args[2].isAmong("--help", "-h")) {
        enforce(args.length == 3, new WrongUsage(errMessages));
        showHelp("create");
        return;
    }
    std.file.chdir(getMountPoint());

    bool flagToAllowSameNames = false;
    args.getopt(
        config.passThrough,
        "allow-same-names|S", &flagToAllowSameNames
    );
    string[] categories = extractOptionRange(args, "-c");
    string[] files = extractOptionRange(args, "-v");
    enforce(categories.empty ^ files.empty, new WrongUsage(errMessages));
    if (!categories.empty) {
        createCategories(categories);
    } else if (!files.empty) {
        createEmtpyFiles(files, flagToAllowSameNames);
    }
}


void createCategories(string[] categories) {
    foreach(cat; categories) {
        auto p = getVitisFileFullPath(cat);
        if (amalthea.fs.exists(p)) {
            printLine!W(
                cat, ": this name is used."._s, " ", "Skipped."._s
            );
            continue;
        }
        createNonExistentCategory(cat);
    }
}


void createEmtpyFiles(string[] vitisFiles, bool allowSameNames) {
    foreach(f; vitisFiles) {
        auto p = getVitisFileFullPath(f);
        if (allowSameNames) {
            f ~= repNameEnding;
        } else if (amalthea.fs.exists(p)) {
            printLine!W(
                f, ": this name is used."._s, " ", "Skipped."._s
            );
            continue;
        }
        std.file.write(p, "");
    }
}
