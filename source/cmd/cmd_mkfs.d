/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2022-2024
 *
 * Distributed under
 * the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module cmd.cmd_mkfs;

// core
import core.sys.posix.sys.types : off_t;
import core.stdc.errno;

// std
static import std.uni;
static import std.file;
static import std.process;
import std.algorithm : predSwitch, canFind;
import std.conv : to;
import std.exception : enforce;
import std.file : FileException, mkdirRecurse, setAttributes;
import std.format : format;
import std.string : empty, replace;
import std.conv : oct = octal;
import std.path : dirName;

// amalthea
static import amalthea.sys;
import amalthea.dataprocessing : extractOptionValue, stripLeft;
import amalthea.fs : exists, FileStat, truncate, getAbsolutePath;
import amalthea.langlocal : _s;
import amalthea.sys : loopctl;

// inner
static import configuration;
import clibase;


immutable wrongMsgForMkfs = "Incorrect use of 'vitis mkfs'.";
immutable seeHelpMsgForMkfs = "See: vitis mkfs --help";

immutable size_t _10G        = 10UL * 1024UL^^3;  // 10 GiB
immutable size_t MIN_FS_SIZE = 64UL * 1024UL^^2;  // 64 MiB


void runMkfsCommand(string[] args) {
    string fsImage = extractOptionValue(args, "-i");  // must be
    string size = extractOptionValue(args, "-s");
    // unexpected arguments must not be in the argument array
    if (fsImage.empty || !args[2 .. $].empty) {
        throw new WrongUsage([wrongMsgForMkfs._s, seeHelpMsgForMkfs._s]);
    }
    try {
        mkfs(fsImage, size.empty ? _10G : parseSize(size));
    } catch (FileException e) {
        catchFileException(e);
    }
}


void catchFileException(FileException e) {
    if (e.errno == EACCES) {
        auto msg = "Privilege escalation is required.\nE: " ~ e.msg;
        throw new CommonError(msg);
    } else {
        throw e;
    }
}


private void mkfs(string image, size_t size) {
    image = getAbsolutePath(image);

    enforce!CommonError(!exists(image), image ~ ": such file exists."._s);
    enforce!WrongUsage(size >= MIN_FS_SIZE, "Minimal size: 64 MiB."._s);
    createVitisFile(image, size);

    uint no = loopctl.getFreeLoopDevice();
    string loopDevice = format!"/dev/loop%u"(no);
    if (!exists(loopDevice)) {
        loopctl.addLoopDevice(no);
    }
    loopctl.associateFileWithDevice(image, loopDevice);
    scope(exit) loopctl.disassociateFileWithDevice(image, loopDevice);
    createExt4Partition(loopDevice);
}


private void createVitisFile(string imagePath, ulong size) {
    ulong freeBytes = std.file.getAvailableDiskSpace(dirName(imagePath));
    enforce!CommonError(size < freeBytes, "Not enough disk space."._s);
    std.file.write(imagePath, "");
    truncate(imagePath, size);
    imagePath.setAttributes(oct!777);
}


/**
 * Parses string with size for a new file.
 * The size string may have a one-letter suffix:
 *     'B' - bytes;
 *     'K' - kibibytes;
 *     'M' - mebibytes;
 *     'G' - gibibytes;
 *     'T' - tebibytes.
 * If string doesn't have any letter in the end,
 * the size will be interpreted as number of bytes.
 *
 * Params:
 *     volume = String view of file size.
 *
 * Returns: integer view of size in bytes.
 */
private ulong parseSize(string volume) {
    auto helpMsg = seeHelpMsgForMkfs._s;
    dchar lastChar = std.uni.toUpper(volume[$-1]);
    ulong size;
    try {
        if (!std.uni.isAlpha(lastChar)) {
            return volume.to!ulong;
        }
        immutable letters = ['B', 'K', 'M', 'G', 'T'];
        immutable errMsgs = ["The size is set incorrectly."._s, helpMsg];
        enforce(canFind(letters, cast(char)lastChar), new WrongUsage(errMsgs));
        volume = volume[0 .. $-1];
        size = lastChar.predSwitch(
            'B', volume.to!ulong,
            'K', to!ulong(volume.to!double * 1024),
            'M', to!ulong(volume.to!double * (2UL^^20)),
            'G', to!ulong(volume.to!double * (2UL^^30)),
            'T', to!ulong(volume.to!double * (2UL^^40)),
            0  // incredible situation
        );
    } catch (Exception) {
        throw new WrongUsage(["The size is set incorrectly."._s, helpMsg]);
    }
    return size;
}


/**
 * Creates ext4 partition on the specified device.
 *
 * Params:
 *     blockDevice = Path to block device.
 */
private void createExt4Partition(string blockDevice) {
    auto res = std.process.execute(["mkfs.ext4", "-m", "0", blockDevice]);
    enforce!CommonError(res.status == 0, format!"mkfs: %s"(res.output));
    string tempDirForDevice = "/tmp/vitis-tmp0";
    for (uint i = 1; exists(tempDirForDevice) && i < uint.max; i++) {
        tempDirForDevice = format!"/tmp/vitis-tmp%u"(i);
    }
    std.file.mkdirRecurse(tempDirForDevice);
    scope(exit) std.file.rmdir(tempDirForDevice);
    amalthea.sys.mount(blockDevice, tempDirForDevice, "ext4");
    scope(exit) amalthea.sys.umount(tempDirForDevice);
    std.file.setAttributes(tempDirForDevice, oct!777);
}
