/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2018-2025
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

module configuration;

static import std.file;
import std.file : exists, isSymlink;
import std.path : baseName;
import std.process : environment;
import std.algorithm : endsWith, startsWith;

import amalthea.fs : FileEntry, FileType;
import amalthea.dataprocessing : sortAssocArrayItemsByValues;

immutable sysDir = "/etc/vitis";


/// Returns primary mount points as keys, main mount points as values.
string[string] getVitisMountPoints() {
    string[string] result;
    auto files = std.file.dirEntries(sysDir, std.file.SpanMode.shallow);
    foreach(string f; files) {
        FileEntry entry;
        try {
            entry = FileEntry(f);
        } catch(std.file.FileException e) {
            continue;  // for cases "Transport endpoint is not connected"
        }
        string name = baseName(entry.path);
        if (!name.startsWith("loop")) {
            continue;
        }
        if (entry.type == FileType.directory && !name.endsWith("-vitis")) {
            string vitisPath = entry.path ~ "-vitis";
            if (!exists(vitisPath)) {
                continue;
            }
            string vitisMountPoint;
            if (vitisPath.isSymlink) {
                vitisMountPoint = std.file.readLink(vitisPath);
            } else if (std.file.isDir(vitisPath)) {
                vitisMountPoint = vitisPath;
            } else {
                continue;
            }
            result[entry.path] = vitisMountPoint;
        }
    }
    return result;
}


string getMountPoint() {
    string mountPoint = environment.get("VTS_MP", "");
    if (mountPoint == "") {
        auto allPoints = sortAssocArrayItemsByValues(getVitisMountPoints());
        if (allPoints.length > 0) {
            mountPoint = allPoints[0][0];
        }
    }
    return mountPoint;
}
