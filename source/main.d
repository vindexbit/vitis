/* This file is part of the Vitis project.
 *
 * Copyright (C) Eugene 'Vindex' Stulin, 2021-2024
 *
 * Distributed under the Boost Software License 1.0 or (at your option)
 * the GNU General Public License 3.0 or later.
 * This file is offered as-is, without any warranty.
 */

import std.stdio : writeln;
import std.csv : csvReader;
import std.range : empty;
import std.string : strip;
import std.typecons : Tuple;

import amalthea.dataprocessing : extractOptionValue, isAmong;
import amalthea.langlocal : _s, initLocalization, getSystemLanguage;

import help : showHelp;

import cmd.cmd_show;
import cmd.cmd_assign;
import cmd.cmd_create;
import cmd.cmd_delete;
import cmd.cmd_mkfs;
import cmd.cmd_mount;

import clibase;


alias Record = Tuple!(string, string, string);

static immutable string[][] translations = () {  // in compile time
    string[][] table;
    string csvText = import("res/translations.csv");
    foreach (record; csvReader!Record(csvText)) {
        table ~= [record[0], record[1], record[2]];
    }
    return table;
}();


/// Prints the software version.
void runVersionCommand() {
    writeln(import("version").strip);
}


/// Main function thats starts CLI commands by arguments.
void parseAndRunCommand(string[] args) {
    string command = args[1];
    if (args.length == 2) {
        if (command == "--version" || command == "version") {
            runVersionCommand();
            return;
        } else if (command.isAmong("--help", "-h", "help")) {
            showHelp();
            return;
        }
    }
    switch(command) {
        case "mkfs":   cmd.cmd_mkfs.runMkfsCommand(args);       break;
        case "mount":  cmd.cmd_mount.runMountCommand(args);     break;
        case "umount": cmd.cmd_mount.runUmountCommand(args);    break;
        case "show":   cmd.cmd_show.runShowCommand(args);       break;
        case "assign": cmd.cmd_assign.runAssignCommand(args);   break;
        case "create": cmd.cmd_create.runCreateCommand(args);   break;
        case "delete": cmd.cmd_delete.runDeleteCommand(args);   break;
        default:
            auto messages = ["Invalid command."._s, "See: vitis --help"._s];
            throw new WrongUsage(messages);
    }
}


int main(string[] args) {
    if (args.length == 1) {
        printLine!(TT.error)("Incorrect use of vitis."._s);
        printLine!(TT.error)("See: vitis --help"._s);
        return WRONG_USAGE;
    }
    try {
        string configFile = extractOptionValue(args, "--conf");
        parseAndRunCommand(args);
    } catch (WrongUsage e) {
        printLine!(TT.error)(e.msg);
        return WRONG_USAGE;
    } catch (CommonError e) {
        printLine!(TT.error)(e.msg);
        return COMMON_ERROR;
    } catch (Exception e) {
        debug {
            printLine!(TT.error)(e);
        } else {
            printLine!(TT.error)(e.msg);
        }
        return COMMON_ERROR;
    }
    return SUCCESS;
}
