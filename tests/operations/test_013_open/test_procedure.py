import os
import sys
sys.path.append('../')

import pytest

import common
from common import get_caseno
from conftest import fs_empty_environment, MP, run_c_test

CATEGORY1 = os.path.join(MP, 'category1')
CATEGORY2 = os.path.join(MP, 'category2')
SUBCATEGORY = os.path.join(CATEGORY2, 'subcategory')
CATEGORY3 = os.path.join(MP, 'category3')

FILE1 = os.path.join(CATEGORY1, 'file1')
FILE2 = os.path.join(CATEGORY1, 'file2')
butI = 'But I being poor have only my dreams\n'
hadI = """Had I the heaven’s embroidered cloths,
Enwrought with golden and silver light,
The blue and the dim of the dark cloths
Of night and light and the half-light,
I would spread the cloths under your feet:
But I being poor have only my dreams;
I have spread my dreams under your feet;
Tread softly, because you tread on my dreams.
"""


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    open()
    """
    os.mkdir(CATEGORY1)
    common.touch(FILE1)
    common.touch(FILE2)
    os.chmod(FILE1, common.MODE_666)  # rw-rw-rw-
    os.chmod(FILE2, common.MODE_555)  # r-xr-xr-x
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    open(), file's note with "\\note"
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, hadI)
    common.write_to_file(FILE1 + '\\note', '')
    run(get_caseno())
