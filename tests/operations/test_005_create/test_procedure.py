import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, DS, run_c_test
import common
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')
FILE1_TXT = os.path.join(CATEGORY1, 'file1.txt')


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    create(), creating regular file
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    create(), wrong using: node in autocategory
    """
    os.mkdir(CATEGORY1)
    common.touch(FILE1_TXT)  # for creating autocategory '@/Extension/txt'
    run(get_caseno())


def test_case03(fs_empty_environment):
    """
    create(), wrong using: path contains unacceptable symbol (LF)
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case04(fs_empty_environment):
    """
    create(), note
    """
    FILE1 = os.path.join(CATEGORY1, 'file1')
    os.mkdir(CATEGORY1)
    common.touch(FILE1)
    run(get_caseno())
