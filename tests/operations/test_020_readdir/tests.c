#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// readdir()
void case01() {
    const char* CATEGORY1 = MP"category1";

    errno = 0;
    DIR* dirstream = opendir(CATEGORY1);
    assert(dirstream != NULL && errno == 0);

    struct dirent* entry;
    int file_counter = 0;
    bool file1_found = false;
    bool file2_found = false;
    bool curr_dir_found = false;  // about "."
    bool parent_dir_found = false;  // about ".."
    entry = readdir(dirstream);
    while(entry != NULL) {
        file_counter++;
        if (0 == strcmp("file1", entry->d_name)) {
            file1_found = true;
        } else if (0 == strcmp("file2", entry->d_name)) {
            file2_found = true;
        } else if (0 == strcmp(".", entry->d_name)) {
            curr_dir_found = true;
        } else if (0 == strcmp("..", entry->d_name)) {
            parent_dir_found = true;
        }
        entry = readdir(dirstream);
    }

    assert(file1_found);
    assert(file2_found);
    assert(curr_dir_found);
    assert(parent_dir_found);
    assert(file_counter == 4);
}


// readdir(), Text @ Spring
// Text has march.txt, april.txt, may.txt, bigtext.txt, yeats.txt
// Spring has march.txt, april.txt, may.txt, spring_file
void case02() {
    const char* REQUEST = "Text\\@\\Spring\\x";
    if (0 != chdir(MP)) {
        perror("chdir");
        assert(0);
    }

    errno = 0;
    DIR* dirstream = opendir(REQUEST);
    if (dirstream == NULL || errno != 0) {
        perror("opendir");
        assert(0);
    }

    struct dirent* entry = NULL;

    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "april.txt"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "bigtext.txt"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "march.txt"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "may.txt"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "spring_file"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "yeats.txt"));
    entry = readdir(dirstream);
    assert(entry == NULL);  // end of file list
}


// readdir(), Text % Spring (intersection)
// Text has march.txt, april.txt, may.txt, bigtext.txt, yeats.txt
// Spring has march.txt, april.txt, may.txt, spring_file
void case03() {
    const char* REQUEST = "Text\\%\\Spring\\x";
    if (0 != chdir(MP)) {
        perror("chdir");
        assert(0);
    }

    errno = 0;
    DIR* dirstream = opendir(REQUEST);
    if (dirstream == NULL || errno != 0) {
        perror("opendir");
        assert(0);
    }

    struct dirent* entry = NULL;

    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "april.txt"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "march.txt"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "may.txt"));
    entry = readdir(dirstream);
    assert(entry == NULL);  // end of file list
}


// readdir(), name list
void case04() {
    const char* NAME_LIST = MP"Poetry/yeats.txt\\names";

    errno = 0;
    DIR* dirstream = opendir(NAME_LIST);
    if (dirstream == NULL || errno != 0) {
        perror("opendir");
        assert(0);
    }

    struct dirent* entry = NULL;
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, ":c2:yeats.txt")); // in Poetry/
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, ":c5:yeats.txt")); // in Text/Fiction/
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, ":c6:yeats.txt")); // in English/
    entry = readdir(dirstream);
    assert(entry == NULL);  // end of file list

    int ret = closedir(dirstream);
    assert(ret == 0);
}
