import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, run_c_test
import common
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')
FILE1 = os.path.join(CATEGORY1, 'file1')
FILE2 = os.path.join(CATEGORY1, 'file2')
butI = 'But I being poor have only my dreams'


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    readdir()
    """
    os.mkdir(CATEGORY1)
    common.touch(FILE1)
    common.touch(FILE2)
    os.chmod(FILE1, common.MODE_666)  # rw-rw-rw-
    os.chmod(FILE2, common.MODE_555)  # r-xr-xr-x
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case02(fs_environment):
    """
    readdir(), Text @ Spring
    """
    run(get_caseno())


def test_case03(fs_environment):
    """
    readdir(), Text % Spring
    """
    run(get_caseno())


def test_case04(fs_extended_environment):
    """
    readdir(), name list
    """
    run(get_caseno())
