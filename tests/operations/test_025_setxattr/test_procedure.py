import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, run_c_test
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    setxattr(), simple case
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    setxattr(), forbidden case
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case03(fs_empty_environment):
    """
    setxattr(), simple case with XATTR_CREATE
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case04(fs_empty_environment):
    """
    setxattr(), error case with XATTR_CREATE
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case05(fs_empty_environment):
    """
    setxattr(), XATTR_REPLACE
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case06(fs_empty_environment):
    """
    setxattr(), error case with XATTR_REPLACE
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())
