#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <sys/xattr.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// removexattr(), simple case
void case01() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    const char* VALUE = "true";
    int ret;

    // prepare with setxattr
    ret = setxattr(CATEGORY1, KEY, VALUE, strlen(VALUE), 0);
    assert(ret == 0);

    // removexattr
    ret = removexattr(CATEGORY1, KEY);
    assert(ret == 0);

    // check with getxattr
    ssize_t value_size = getxattr(CATEGORY1, KEY, NULL, 0);
    assert(value_size == -1 && errno == ENODATA);
}


// removexattr(), non-existent key
void case02() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "user.mark.future";
    int ret;

    ret = removexattr(CATEGORY1, KEY);
    assert(ret == -1 && errno == ENODATA);
}


// removexattr(), system key
void case03() {
    const char* CATEGORY1 = MP"category1";
    const char* KEY = "vitis.canon_name";
    int ret;

    ret = removexattr(CATEGORY1, KEY);
    assert(ret == -1 && errno == EPERM);
}
