#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <sys/xattr.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// getxattr(), canon name
void case01() {
    const char* CATEGORY1 = MP"category1";

    ssize_t value_size = getxattr(CATEGORY1, "vitis.canon_name", NULL, 0);
    if (value_size == -1) {
        perror("getxattr");
        assert(0);
    }
    char* value = malloc(value_size + 1);
    memset(value, '\0', value_size + 1);
    value_size = getxattr(CATEGORY1, "vitis.canon_name", value, value_size);
    if (value_size == -1) {
        perror("getxattr");
        assert(0);
    }
    assert(0 == strcmp("category1", value));
}


// getxattr(), non-existent key
void case02() {
    const char* CATEGORY1 = MP"category1";
    ssize_t value_size = getxattr(CATEGORY1, "user.strange_key", NULL, 0);
    assert(value_size == -1 && errno == ENODATA);
}
