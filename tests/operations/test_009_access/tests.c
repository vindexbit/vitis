#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// access(), checking permissions
void case01() {
    int ret;

    const char* FILE1 = MP"category1/file1";
    errno = 0;
    ret = access(FILE1, F_OK);
    assert(ret == 0);  // file exists
    errno = 0;
    ret = access(FILE1, R_OK);
    assert(ret == 0);  // file grants read permission
    errno = 0;
    ret = access(FILE1, W_OK);
    assert(ret == 0);  // file grants write permission
    errno = 0;
    ret = access(FILE1, X_OK);
    // file doesn't grant execution permission
    assert(ret == -1 && errno == EACCES);

    const char* FILE2 = MP"category1/file2";
    errno = 0;
    ret = access(FILE2, F_OK);
    assert(ret == 0); // file exists
    errno = 0;
    ret = access(FILE2, R_OK);
    assert(ret == 0); // file grants read permission
    errno = 0;
    ret = access(FILE2, W_OK);
    // file doesn't grants write permission
    if (0 == getuid())  {  //root
        assert(ret == 0);
    } else {
        assert(ret == -1 && errno == EACCES);
    }
    errno = 0;
    ret = access(FILE2, X_OK);
    assert(ret == 0); // file grants execution permission
}
