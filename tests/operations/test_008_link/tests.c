#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();
void case05();
void case06();
void case07();
void case08();
void case09();
void case10();
void case11();
void case12();
void case13();
void case14();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        case 5: case05(); break;
        case 6: case06(); break;
        case 7: case07(); break;
        case 8: case08(); break;
        case 9: case09(); break;
        case 10: case10(); break;
        case 11: case11(); break;
        case 12: case12(); break;
        case 13: case13(); break;
        case 14: case14(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// link(), link without changing the current directory
void case01() {
    const char* FILE1 = MP"category1/file1";
    const char* LINK1 = MP"category1/link1";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";
    const char* FILE1_CATS      = DS".vitis/00/00/00/00/03/categories";
    const char* FILE1_ATTRS     = DS".vitis/00/00/00/00/03/attrs";

    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");  // default nlink
    if (-1 == link(FILE1, LINK1)) {
        perror("link");
        assert(0);
    }
    struct stat st_orig, st_link;
    assert(-1 != lstat(FILE1, &st_orig));
    assert(-1 != lstat(LINK1, &st_link));
    assert(st_orig.st_ino == st_link.st_ino);
    assert(st_orig.st_nlink == st_link.st_nlink);

    check_dds_file(FILE1_CATS, 2, "file1", 1, 2);  // 1st record
    check_dds_file(FILE1_CATS, 2, "link1", 2, 2);  // 2nd record
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "3");
    check_dds_file(CATEGORY1_NODES, 3, "file1", 1, 2);  // 1st record
    check_dds_file(CATEGORY1_NODES, 3, "link1", 2, 2);  // 2nd record
}


// link(), link in the other directory
void case02() {
    const char* FILE1 = MP"category1/file1";
    const char* LINK1 = MP"category1/link1";
    const char* LINK2 = MP"category2/link2";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY2_NODES = DS".vitis/00/00/00/00/03/nodes";
    const char* FILE1_CATS      = DS".vitis/00/00/00/00/04/categories";
    const char* FILE1_ATTRS     = DS".vitis/00/00/00/00/04/attrs";

    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");  // default nlink
    if (-1 == link(FILE1, LINK1)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "3");
    if (-1 == link(FILE1, LINK2)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "4");

    check_dds_file(FILE1_CATS, 2, "file1", 1, 3);  // 1st record
    check_dds_file(FILE1_CATS, 2, "link1", 2, 3);  // 2nd record
    check_dds_file(FILE1_CATS, 3, "link2", 3, 3);  // 3rd record
    check_dds_file(CATEGORY1_NODES, 4, "file1", 1, 2);  // 1st record
    check_dds_file(CATEGORY1_NODES, 4, "link1", 2, 2);  // 2nd record
    check_dds_file(CATEGORY2_NODES, 4, "link2", 1, 1);  // single record
}


// link(), two new links in the same directory
void case03() {
    const char* FILE1 = MP"category1/file1";
    const char* LINK1 = MP"category1/link1";
    const char* LINK2 = MP"category1/link2";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";
    const char* FILE1_CATS      = DS".vitis/00/00/00/00/03/categories";
    const char* FILE1_ATTRS     = DS".vitis/00/00/00/00/03/attrs";

    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");  // default nlink
    if (-1 == link(FILE1, LINK1)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "3");
    if (-1 == link(FILE1, LINK2)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "4");

    check_dds_file(FILE1_CATS, 2, "file1", 1, 3);  // 1st record
    check_dds_file(FILE1_CATS, 2, "link1", 2, 3);  // 2nd record
    check_dds_file(FILE1_CATS, 2, "link2", 3, 3);  // 3rd record
    check_dds_file(CATEGORY1_NODES, 3, "file1", 1, 3);  // 1st record
    check_dds_file(CATEGORY1_NODES, 3, "link1", 2, 3);  // 2nd record
    check_dds_file(CATEGORY1_NODES, 3, "link2", 3, 3);  // 2nd record
}


// link(), additional five links
void case04() {
    const char* FILE1 = MP"category1/file1";
    const char* LINK1 = MP"category1/link1";
    const char* LINK2 = MP"category1/link2";
    const char* LINK3 = MP"category2/link3";
    const char* LINK4 = MP"category2/link4";
    const char* LINK5 = MP"category2/link5";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY2_NODES = DS".vitis/00/00/00/00/03/nodes";
    const char* FILE1_CATS      = DS".vitis/00/00/00/00/04/categories";
    const char* FILE1_ATTRS     = DS".vitis/00/00/00/00/04/attrs";

    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");  // default nlink
    if (-1 == link(FILE1, LINK1)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "3");
    if (-1 == link(FILE1, LINK2)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "4");
    if (-1 == link(FILE1, LINK3)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "5");
    if (-1 == link(FILE1, LINK4)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "6");
    if (-1 == link(FILE1, LINK5)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "7");

    check_dds_file(FILE1_CATS, 2, "file1", 1, 6);  // 1st record
    check_dds_file(FILE1_CATS, 2, "link1", 2, 6);  // 2nd record
    check_dds_file(FILE1_CATS, 2, "link2", 3, 6);  // 3rd record
    check_dds_file(FILE1_CATS, 3, "link3", 4, 6);  // 4th record
    check_dds_file(FILE1_CATS, 3, "link4", 5, 6);  // 5th record
    check_dds_file(FILE1_CATS, 3, "link5", 6, 6);  // 6th record
    check_dds_file(CATEGORY1_NODES, 4, "file1", 1, 3);  // 1st record
    check_dds_file(CATEGORY1_NODES, 4, "link1", 2, 3);  // 2nd record
    check_dds_file(CATEGORY1_NODES, 4, "link2", 3, 3);  // 3rd record
    check_dds_file(CATEGORY2_NODES, 4, "link3", 1, 3);  // 1st record
    check_dds_file(CATEGORY2_NODES, 4, "link4", 2, 3);  // 2nd record
    check_dds_file(CATEGORY2_NODES, 4, "link5", 3, 3);  // 3rd record
}


// link(), repeated name and repeated inode
void case05() {
    const char* FILE1 = MP"category1/file1";
    const char* REQUEST1 = MP"category1/file1";
    const char* REQUEST2 = MP"category1/file1\\r";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";
    const char* FILE1_CATS      = DS".vitis/00/00/00/00/03/categories";
    const char* FILE1_ATTRS     = DS".vitis/00/00/00/00/03/attrs";

    void check_rep_name_in_single_dir(const char* newpath) {
        check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");  // default nlink
        assert(-1 == link(FILE1, REQUEST1));  // invalid operation
        assert(errno == EEXIST);
        check_dds_file(FILE1_CATS, 2, "file1", 1, 1);  // single record
        check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");
        check_dds_file(CATEGORY1_NODES, 3, "file1", 1, 1);  // single record
    }
    // without the "\\r" suffix
    check_rep_name_in_single_dir(REQUEST1);
    // with the "\\r" suffix (same behavior)
    check_rep_name_in_single_dir(REQUEST2);
}


// link(), repeated name, complicated case
void case06() {
    const char* FILE1 = MP"category1/file1";
    const char* FILE2 = MP"category2/yeats_quote";
    const char* REQUEST1_SIMPLE = MP"category2/yeats_quote";
    const char* REQUEST2_REPSUF = MP"category2/yeats_quote\\r";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY2_NODES = DS".vitis/00/00/00/00/03/nodes";
    const char* FILE1_ATTRS     = DS".vitis/00/00/00/00/04/attrs";
    const char* FILE1_CATS      = DS".vitis/00/00/00/00/04/categories";

    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "2");  // default nlink
    assert(-1 == link(FILE1, REQUEST1_SIMPLE));  // invalid operation
    assert(errno == EEXIST);

    if (0 != link(FILE1, REQUEST2_REPSUF)) {
        perror("link");
        assert(0);
    }
    check_attr(FILE1_ATTRS, "vitis.stat.nlink", "3");
    check_dds_file(FILE1_CATS, 2, "file1", 1, 2);  // 1st record
    check_dds_file(FILE1_CATS, 3, "yeats_quote", 2, 2);  // 2nd record
    check_dds_file(CATEGORY1_NODES, 4, "file1", 1, 1);  // single record
    check_dds_file(CATEGORY2_NODES, 5, "yeats_quote", 1, 2);  // 1st record
    check_dds_file(CATEGORY2_NODES, 4, "yeats_quote", 2, 2);  // 2nd record
}


// link(), many links
void case07() {
    const char* FILE1 = MP"category1/file1";
    const size_t LIMIT_NLINKS = 4096;
    char new_link[256] = {0};
    // file has two links by default; new link number is equal to 3
    for (size_t i = 3; i <= LIMIT_NLINKS; i++) {
        sprintf(new_link, MP"category1/link%zu", i);
        if (0 != link(FILE1, new_link)) {
            perror("link");
            assert(0);
        }
    }
    sprintf(new_link, MP"category1/link%zu", (size_t)LIMIT_NLINKS + (size_t)1);
    assert(-1 == link(FILE1, new_link));
    assert(errno == EMLINK);
}


// link(), invalid characters
void case08() {
    const char* FILE1 = MP"category1/file1";
    const char* REQUEST1 = MP"category1/link\n";
    const char* CATEGORY1_NODES = DS".vitis/00/00/00/00/02/nodes";

    int ret = link(FILE1, REQUEST1);
    assert(-1 == ret && errno == EINVAL);
    check_dds_file(CATEGORY1_NODES, 3, "file1", 1, 1);  // single record
}


// link(), autocategories, error case
void case09() {
    const char* FILE2 = MP"category1/file2";
    const char* REQUEST1_ERR = MP"@/Extension/txt/experiment1";
    int ret = link(FILE2, REQUEST1_ERR);
    assert(-1 == ret && errno == EPERM);
}


// link(), autocategories, normal case
void case10() {
    const char* FILE1_IN_AUTOCATEGORY = MP"@/Extension/txt/file1.txt";
    const char* REQUEST1_OK = MP"category1/link1";
    errno = 0;
    int ret = link(FILE1_IN_AUTOCATEGORY, REQUEST1_OK);
    assert(0 == ret && errno == 0);
}


// link(), link to autocategory, error case
void case11() {
    const char* AUTOCATEGORY = MP"@/Extension/txt/";
    int ret = link(AUTOCATEGORY, MP"link");
    // Linux prohibits creating hard links to directories
    assert(ret == -1 && errno == EPERM);

    const char* IM_LINK = MP"@/Extension/txt/\\l";
    ret = link(IM_LINK, MP"link");
    assert(ret == -1 && errno == EPERM);
}


// link(), link to root
void case12() {
    int ret = link(MP, MP"link");
    assert(ret == -1 && errno == EPERM);
}


// link(), link to category (imaginary link)
void case13() {
    const char* CAT1 = MP"category1";
    const char* CAT1_IMLINK = MP"category1\\l";
    const char* CAT1_LINK = MP"category1_link";
    int ret = link(CAT1_IMLINK, CAT1_LINK);
    if (ret == -1) {
        perror("link");
        assert(0);
    }
}


// link(), link to category (imaginary link), loop situation
void case14() {
    const char* CAT1 = MP"category1";
    const char* CAT1_IMLINK = MP"category1\\l";
    const char* CAT1_LOOP_LINK = MP"category1/category1_link";

    int ret = link(CAT1_IMLINK, CAT1_LOOP_LINK);
    assert(ret == -1 && errno == ELOOP);

    CAT1_LOOP_LINK = MP"category1/subcat/subsubcat/category1_link";
    errno = 0;
    int ret2 = link(CAT1_IMLINK, CAT1_LOOP_LINK);
    assert(ret == -1 && errno == ELOOP);
}
