import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, DS, run_c_test
import common
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')

CATEGORY2 = os.path.join(MP, 'category2')
FILE1 = os.path.join(CATEGORY1, 'file1')

butI = 'But I being poor have only my dreams\n'
iHave = 'I have spread my dreams under your feet\n'


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    link(), link without changing the current directory
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    link(), link in the other directory
    """
    os.mkdir(CATEGORY1)
    os.mkdir(CATEGORY2)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case03(fs_empty_environment):
    """
    link(), two new links in the same directory
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case04(fs_empty_environment):
    """
    link(), additional five links
    """
    os.mkdir(CATEGORY1)
    os.mkdir(CATEGORY2)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case05(fs_empty_environment):
    """
    link(), repeated name and repeated inode
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case06(fs_empty_environment):
    """
    link(), repeated name, complicated case
    """
    FILE2 = os.path.join(CATEGORY2, 'yeats_quote')

    os.mkdir(CATEGORY1)
    os.mkdir(CATEGORY2)
    common.write_to_file(FILE1, butI)
    common.write_to_file(FILE2, iHave)
    run(get_caseno())


@pytest.mark.skip(reason='A long test')
def test_case07(fs_empty_environment):
    """
    link(), many links
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case08(fs_empty_environment):
    """
    link(), invalid characters
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case09(fs_empty_environment):
    """
    link(), autocategories, error case
    """
    FILE1_TXT = os.path.join(CATEGORY1, 'file1.txt')
    FILE2 = os.path.join(CATEGORY1, 'file2')
    os.mkdir(CATEGORY1)
    common.touch(FILE1_TXT)  # for creating autocategory '@/Extension/txt'
    common.write_to_file(FILE2, butI)
    run(get_caseno())


def test_case10(fs_empty_environment):
    """
    link(), autocategories, normal case
    """
    FILE1_TXT = os.path.join(CATEGORY1, 'file1.txt')
    os.mkdir(CATEGORY1)
    common.touch(FILE1_TXT)  # for creating autocategory '@/Extension/txt'
    run(get_caseno())


def test_case11(fs_empty_environment):
    """
    link(), link to autocategory, error case
    """
    FILE1_TXT = os.path.join(CATEGORY1, 'file1.txt')
    os.mkdir(CATEGORY1)
    common.touch(FILE1_TXT)  # for creating autocategory '@/Extension/txt'
    run(get_caseno())


def test_case12(fs_empty_environment):
    """
    link(), link to root
    """
    run(get_caseno())


def test_case13(fs_empty_environment):
    """
    link(), link to category (imaginary link)
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case14(fs_empty_environment):
    """
    link(), link to category (imaginary link), loop situation
    """
    os.mkdir(CATEGORY1)
    SUBCAT = os.path.join(CATEGORY1, "subcat")
    os.mkdir(SUBCAT)
    SUBSUBCAT = os.path.join(SUBCAT, "subsubcat")
    os.mkdir(SUBSUBCAT)
    run(get_caseno())
