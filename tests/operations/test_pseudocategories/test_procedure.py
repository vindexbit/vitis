import os
import sys
sys.path.append('../')

import pytest

import common
from common import get_caseno
from conftest import MP, run_c_test
from conftest import fs_empty_environment

CATEGORY1 = os.path.join(MP, 'category1')
FILE = os.path.join(CATEGORY1, 'file')
TXT_FILE = os.path.join(CATEGORY1, 'file.txt')
MP3_FILE = os.path.join(CATEGORY1, 'file.mp3')
PDF_FILE = os.path.join(CATEGORY1, 'file.pdf')


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    os.mkdir(CATEGORY1)
    common.touch(FILE)
    common.touch(TXT_FILE)
    common.touch(MP3_FILE)
    run(get_caseno())


def test_case02(fs_empty_environment):
    os.mkdir(CATEGORY1)
    common.write_bytes_to_file(MP3_FILE, b'\x49\x44\x33')
    common.write_bytes_to_file(PDF_FILE, b'\x25\x50\x44\x46')
    run(get_caseno())


def test_case03(fs_empty_environment):
    run(get_caseno())
