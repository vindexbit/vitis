#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


#define SIMPLE_FILE MP"category1/file"
#define TXT_FILE    MP"category1/file.txt"
#define MP3_FILE    MP"category1/file.mp3"
#define PDF_FILE    MP"category1/file.pdf"

// extension
void case01() {
    assert(0 == access(SIMPLE_FILE, F_OK));
    assert(0 == access(TXT_FILE, F_OK));
    assert(0 == access(MP3_FILE, F_OK));
    struct stat simple_file_st, txt_file_st, mp3_file_st;
    lstat(SIMPLE_FILE, &simple_file_st);
    lstat(TXT_FILE, &txt_file_st);
    lstat(MP3_FILE, &mp3_file_st);
    int ret;

    struct stat cat_inode;

    // "Extension" in data storage
    chdir(DS".vitis");
    assert(0 == access("@", F_OK));
    assert(0 == access("@/st_ino", F_OK));
    assert(0 == access("@/Extension", F_OK));
    assert(0 == access("@/Extension/st_ino", F_OK));
    chdir("../..");

    // "Extension/txt" in data storage
    chdir(DS".vitis");
    assert(0 == access("@/Extension/txt", F_OK));
    assert(0 == access("@/Extension/txt/st_ino", F_OK));
    chdir("../..");

    // "Extension/mp3" in data storage
    chdir(DS".vitis");
    assert(0 == access("@/Extension/mp3", F_OK));
    assert(0 == access("@/Extension/mp3/st_ino", F_OK));
    chdir("../..");

    // check records in file attributes
    char attr_file[1024];

    memset(attr_file, '\0', 1024);
    sprintf(attr_file, DS".vitis/00/00/00/00/%02x/attrs", txt_file_st.st_ino);
    assert(check_attr(attr_file, "vitis.apc.extension", "txt"));

    memset(attr_file, '\0', 1024);
    sprintf(attr_file, DS".vitis/00/00/00/00/%02x/attrs", mp3_file_st.st_ino);
    assert(check_attr(attr_file, "vitis.apc.extension", "mp3"));
}


// format
void case02() {
    assert(0 == access(MP3_FILE, F_OK));
    assert(0 == access(PDF_FILE, F_OK));
    struct stat mp3_file_st, pdf_file_st;
    lstat(MP3_FILE, &mp3_file_st);
    lstat(PDF_FILE, &pdf_file_st);
    int ret;

    // "Format" in data storage
    chdir(DS".vitis");
    assert(0 == access("@", F_OK));
    assert(0 == access("@/Format", F_OK));
    assert(0 == access("@/Format/st_ino", F_OK));
    chdir("../..");

    // "Format/MP3" in data storage
    chdir(DS".vitis");
    assert(0 == access("@/Format/MP3", F_OK));
    assert(0 == access("@/Format/MP3/st_ino", F_OK));
    chdir("../..");

    // "Format/PDF" in data storage
    chdir(DS".vitis");
    assert(0 == access("@/Format/PDF", F_OK));
    assert(0 == access("@/Format/PDF/st_ino", F_OK));
    chdir("../..");

    // check records in file attributes
    char attr_file[1024];

    memset(attr_file, '\0', 1024);
    sprintf(attr_file, DS".vitis/00/00/00/00/%02x/attrs", mp3_file_st.st_ino);
    assert(check_attr(attr_file, "vitis.apc.format", "MP3"));

    memset(attr_file, '\0', 1024);
    sprintf(attr_file, DS".vitis/00/00/00/00/%02x/attrs", pdf_file_st.st_ino);
    assert(check_attr(attr_file, "vitis.apc.format", "PDF"));
}


/* path lists

   A complex hierarchy of directories is formed for testing:

                      ┌──────────────┐
                      │   fs root    │
                      │              │◄────┐
                      │              │     │
                 ┌───►│              │◄──┐ │
                 │    │      1       │   │ │
                 │    └──────────────┘   │ └─────────────┐
┌──────────┬───┐ │    ┌──────────┬───┐   │  ┌──────────┬─┴─┐
│category2 │ 1 ├─┘    │category3 │ 1 ├───┘  │category4 │ 1 │
├──────────┴───┤      ├──────────┴───┤      ├──────────┴───┤
│              │      │              │      │              │
│              │◄──┐  │              │◄─┐   │              │◄─┐
│      2       │   │  │      3       │  │   │      4       │  │
└──────────────┘   │  └──────────────┘  │   └──────────────┘  │
                   │                    └─────┐               │
                   │             ┌──────────┬─┴─┐             │
      ┌──────────┬─┴─┐           │category5 │ 3 │             │
      │category6 │ 2 │           ├──────────┼───┤             │
      ├──────────┼───┤           │category5 │ 4 ├─────────────┘
      │c6_altname│ 5 ├──────┐    ├──────────┴───┤
      ├──────────┴───┤      └───►│      5       │
      │      6       │           └──────────────┘
      └──────────────┘
*/
void case03() {
    const char* category2 = MP"category2";
    const char* category3 = MP"category3";
    const char* category4 = MP"category4";
    const char* category5 = MP"category3/category5";
    const char* category5_imlink = MP"category3/category5\\l";
    const char* category5_2nd = MP"category4/category5";
    const char* category6 = MP"category2/category6";
    const char* category6_imlink = MP"category2/category6\\l";
    const char* category6_2nd = MP"category3/category5/c6_altname";

    /*
     * preparation
     */
    void create_and_check_dir(const char* dir, ino_t expected_ino) {
        mkdir(dir, 0777);
        struct stat st;
        assert(0 == lstat(dir, &st));
        assert(st.st_ino == expected_ino);
    }
    create_and_check_dir(category2, 2);
    create_and_check_dir(category3, 3);
    create_and_check_dir(category4, 4);
    create_and_check_dir(category5, 5);
    create_and_check_dir(category6, 6);

    assert(0 == link(category5_imlink, category5_2nd));
    assert(0 == link(category6_imlink, category6_2nd));

    /*
     * test
     */
    const char* cat6_path_list = MP"category2/category6\\paths";
    int ret;
    struct stat pl_st;
    ret = lstat(cat6_path_list, &pl_st);
    assert((pl_st.st_mode & S_IFMT) == S_IFDIR);

    DIR* dirstream = opendir(cat6_path_list);
    if (dirstream == NULL || errno != 0) {
        perror("opendir");
        assert(0);
    }
    struct dirent* entry = NULL;
    entry = readdir(dirstream);
    /* assert(0 == strcmp(entry->d_name, "1")); */
    strcmp_assert(entry->d_name, "1");
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "2"));
    entry = readdir(dirstream);
    assert(0 == strcmp(entry->d_name, "3"));
    entry = readdir(dirstream);
    assert(entry == NULL);  // end of file list

    // links
    const char* path1 = MP"category2/category6\\paths/1";
    const char* path2 = MP"category2/category6\\paths/2";
    const char* path3 = MP"category2/category6\\paths/3";
    char buffer[256] = {0};

    assert(-1 != readlink(path1, buffer, 256));
    strcmp_assert(buffer, "category2/category6");
    memset(buffer, '\0', 256);

    assert(-1 != readlink(path2, buffer, 256));
    strcmp_assert(buffer, "category3/category5/c6_altname");
    memset(buffer, '\0', 256);

    assert(-1 != readlink(path3, buffer, 256));
    strcmp_assert(buffer, "category4/category5/c6_altname");
    memset(buffer, '\0', 256);
    
    /*
     * cleanup
     */
    rmdir(category2);
    rmdir(category3);
    rmdir(category4);
    rmdir(category5);
    rmdir(category6);
    rmdir(category5_2nd);
    rmdir(category6_2nd);
}
