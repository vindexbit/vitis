import os
import sys
sys.path.append('../')

import pytest

import common
from common import get_caseno
from conftest import MP, run_c_test
from conftest import fs_empty_environment, fs_extended_environment

CATEGORY1 = os.path.join(MP, 'category1')
FILE1 = os.path.join(CATEGORY1, 'file1')
butI = 'But I being poor have only my dreams\n'


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    unlink(), deleting file
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    unlink(), deleting hard link to directory
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())


def test_case03(fs_extended_environment):
    """
    unlink(), file from name list
    """
    run(get_caseno())
