#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// rmdir(), one empty category
void case01() {
    const char* CATEGORY1 = MP"category1";
    assert(0 == access(CATEGORY1, 0));

    // rmdir
    if (-1 == rmdir(CATEGORY1)) {
        perror("rmdir");
        assert(0);
    }
    assert(0 != access(CATEGORY1, 0));
}


// rmdir(), wrong situation: one non-empty category
void case02() {
    const char* CATEGORY1 = MP"category1";
    assert(0 == access(CATEGORY1, 0));

    // rmdir, unsuccessfull attempt
    errno = 0;
    int ret = rmdir(CATEGORY1);
    assert(ret = -1 && errno == ENOTEMPTY);
}


// rmdir(), hard link to directory
void case03() {
    const char* CATEGORY1 = MP"category1";
    const char* CATEGORY1_IMLINK = MP"category1\\l";
    const char* CATEGORY1_2ND = MP"category1_2nd";
    const char* FILE1 = MP"category1/file1";
    struct stat st;
    assert(0 == stat(CATEGORY1, &st));
    assert(st.st_nlink == 2);  // default primary value
    assert(0 == link(CATEGORY1_IMLINK, CATEGORY1_2ND));
    assert(0 == stat(CATEGORY1, &st));
    assert(st.st_nlink == 3);

    // test
    assert(0 == rmdir(CATEGORY1_2ND));
    assert(0 == stat(CATEGORY1, &st));
    assert(st.st_nlink == 2);
    assert(-1 == rmdir(CATEGORY1) && errno == ENOTEMPTY);
    assert(0 == unlink(FILE1));
    assert(0 == rmdir(CATEGORY1));
}
