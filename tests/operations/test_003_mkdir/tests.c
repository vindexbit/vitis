#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// mkdir(), new category
void case01() {
    const char* CATEGORY1         = MP"category1/";
    const char* CATEGORY1_IN_DS   = DS".vitis/00/00/00/00/02/";
    const char* CATEGORY1_ATTRS   = DS".vitis/00/00/00/00/02/attrs";
    const char* CATEGORY1_CATS    = DS".vitis/00/00/00/00/02/categories";
    const char* CATEGORY1_SUBCATS = DS".vitis/00/00/00/00/02/subcategories";
    const char* CATEGORY1_NODES   = DS".vitis/00/00/00/00/02/nodes";
    const char* CATEGORY1_FILE    = DS".vitis/00/00/00/00/02/file";
    const char* ROOT_SUBCATS      = DS".vitis/00/00/00/00/01/subcategories";

    assertZero4KiB(ROOT_SUBCATS);

    if (-1 == mkdir(CATEGORY1, 0777)) {
        perror("mkdir");
        assert(0);
    }

    assert(0 == access(CATEGORY1, 0));
    assert(0 == access(CATEGORY1_IN_DS, 0));
    assert(0 == access(CATEGORY1_ATTRS, 0));
    assert(0 == access(CATEGORY1_CATS, 0));
    assert(0 == access(CATEGORY1_SUBCATS, 0));
    assert(0 == access(CATEGORY1_NODES, 0));
    assert(0 != access(CATEGORY1_FILE, 0));  // no file

    assertZero4KiB(CATEGORY1_SUBCATS);
    assertZero4KiB(CATEGORY1_NODES);

    check_dds_file(CATEGORY1_CATS, 1, "category1", 1, 1);
    check_dds_file(ROOT_SUBCATS, 2, "category1", 1, 1);
    check_attrs(CATEGORY1_ATTRS, 'd', "category1");
}


// mkdir(), wrong using: category with strange symbols
void case02() {
    const char* CATEGORY1 = MP"catego\ry1";
    const char* ROOT_SUBCATS = DS".vitis/00/00/00/00/01/subcategories";
    errno = 0;
    int ret = mkdir(CATEGORY1, 0777);
    assert(errno == EINVAL);
    assert(-1 == ret);
    assert(-1 == access(DS".vitis/00/00/00/00/02/", 0));
    assertZero4KiB(ROOT_SUBCATS);
}


// mkdir(), wrong using: new autocategory
void case03() {
    const char* CATEGORY1 = MP"@/Extension/category1";
    errno = 0;
    int ret = mkdir(CATEGORY1, 0777);
    assert(errno == EACCES);
    assert(-1 == ret);
    assert(0 != CATEGORY1);
}


// mkdir(), new category and new subcategory
void case04() {
    const char* CATEGORY1            = MP"category1"; 
    const char* CATEGORY1_IN_DS      = DS".vitis/00/00/00/00/02/";
    const char* CATEGORY1_ATTRS      = DS".vitis/00/00/00/00/02/attrs";
    const char* CATEGORY1_CATS       = DS".vitis/00/00/00/00/02/categories";
    const char* CATEGORY1_SUBCATS    = DS".vitis/00/00/00/00/02/subcategories";
    const char* CATEGORY1_NODES      = DS".vitis/00/00/00/00/02/nodes";
    const char* SUBCATEGORY1         = MP"category1/subcat1";
    const char* SUBCATEGORY1_IN_DS   = DS".vitis/00/00/00/00/03/";
    const char* SUBCATEGORY1_ATTRS   = DS".vitis/00/00/00/00/03/attrs";
    const char* SUBCATEGORY1_CATS    = DS".vitis/00/00/00/00/03/categories";
    const char* SUBCATEGORY1_SUBCATS = DS".vitis/00/00/00/00/03/subcategories";
    const char* SUBCATEGORY1_NODES   = DS".vitis/00/00/00/00/03/nodes";
    const char* ROOT_SUBCATS         = DS".vitis/00/00/00/00/01/subcategories";
    const ino_t ROOT_INO = 1;
    const ino_t CATEGORY1_INO = 2;
    const ino_t SUBCATEGORY1_INO = 3;

    // category1

    assertZero4KiB(ROOT_SUBCATS);
    if (-1 == mkdir(CATEGORY1, 0777)) {
        perror("mkdir");
        fprintf(stderr, "\n");
        assert(0);
    }
    assert(0 == access(CATEGORY1, 0));
    assert(0 == access(CATEGORY1_IN_DS, 0));
    assertZero4KiB(CATEGORY1_SUBCATS);
    assertZero4KiB(CATEGORY1_NODES);
    check_dds_file(CATEGORY1_CATS, ROOT_INO, "category1", 1, 1);
    check_dds_file(ROOT_SUBCATS, CATEGORY1_INO, "category1", 1, 1);
    check_attrs(CATEGORY1_ATTRS, 'd', "category1");

    // subcat1

    if (-1 == mkdir(SUBCATEGORY1, 0777)) {
        perror("mkdir for subcategory");
        fprintf(stderr, "\n");
        assert(0);
    }
    assert(0 == access(SUBCATEGORY1, 0));
    assert(0 == access(SUBCATEGORY1_IN_DS, 0));
    assertZero4KiB(SUBCATEGORY1_NODES);
    check_dds_file(SUBCATEGORY1_CATS, CATEGORY1_INO, "subcat1", 1, 1);
    check_attrs(CATEGORY1_ATTRS, 'd', "category1"); 
    check_attrs(SUBCATEGORY1_ATTRS, 'd', "subcat1"); 
}

