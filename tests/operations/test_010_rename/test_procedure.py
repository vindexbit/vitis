import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, run_c_test, MP
import common
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')
FILE1 = os.path.join(CATEGORY1, 'file1')
FILE2 = os.path.join(CATEGORY1, 'file2')
butI = 'But I being poor have only my dreams\n'

CATEGORY2 = os.path.join(MP, 'category2')
CAT2_FILE2 = os.path.join(CATEGORY2, 'file2')


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    rename(), one regular file
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    rename(), one non-empty directory
    """
    os.mkdir(CATEGORY1)
    common.write_to_file(FILE1, butI)
    run(get_caseno())


def test_case03(fs_empty_environment):
    """
    rename(), one directory to existent non-empty directory
    """
    os.mkdir(CATEGORY1)
    os.mkdir(CATEGORY2)
    common.write_to_file(FILE1, butI)
    common.touch(CAT2_FILE2)
    run(get_caseno())


def test_case04(fs_extended_environment):
    """
    rename(), name list, impossible operation
    """
    run(get_caseno())


def test_case05(fs_extended_environment):
    """
    rename(), file from name list
    """
    run(get_caseno())
