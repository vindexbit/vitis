#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "../../common.h"


void case01();
void case02();
void case03();
void case04();
void case05();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        case 2: case02(); break;
        case 3: case03(); break;
        case 4: case04(); break;
        case 5: case05(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


// rename(), one regular file
void case01() {
    const char* FILE1 = MP"category1/file1";
    const char* FILE2 = MP"category1/file2";

    struct stat st1, st2;
    if (-1 == lstat(FILE1, &st1)) {
        perror("lstat");
        assert(0);
    }
    if (-1 == rename(FILE1, FILE2)) {
        perror("rename");
        assert(0);
    }
    if (-1 == lstat(FILE2, &st2)) {
        perror("lstat");
        assert(0);
    }
    assert(0 != access(FILE1, F_OK));
    assert(0 == access(FILE2, F_OK));
    assert(st1.st_size == st2.st_size);
    assert(st1.st_ino == st2.st_ino);
}


// rename(), one non-empty directory
void case02() {
    const char* CATEGORY1 = MP"category1";
    const char* CATEGORY2 = MP"category2";
    const char* FILE1 = MP"category1/file1";
    const char* FILE1_IN_NEW_LOCATION = MP"category2/file1";

    struct stat st1, st2;
    if (-1 == lstat(CATEGORY1, &st1)) {
        perror("lstat");
        assert(0);
    }

    if (-1 == rename(CATEGORY1, CATEGORY2)) {
        perror("rename");
        assert(0);
    }
    if (-1 == lstat(CATEGORY2, &st2)) {
        perror("lstat");
        assert(0);
    }
    assert(0 != access(CATEGORY1, F_OK));
    assert(0 == access(CATEGORY2, F_OK));
    assert(0 != access(FILE1, F_OK));
    assert(0 == access(FILE1_IN_NEW_LOCATION, F_OK));
    assert(st1.st_ino == st2.st_ino);
}


// rename(), one directory to existent non-empty directory
void case03() {
    const char* CATEGORY1 = MP"category1";
    const char* CATEGORY2 = MP"category2";
    const char* FILE1 = MP"category1/file1";
    const char* FILE2 = MP"category2/file2";
    const char* FILE1_IN_NEW_LOCATION = MP"category2/file1";

    struct stat st1;
    if (-1 == lstat(CATEGORY1, &st1)) {
        perror("lstat");
        assert(0);
    }
    int ret = rename(CATEGORY1, CATEGORY2);
    assert(ret == -1 && errno == ENOTEMPTY);
    assert(0 == access(CATEGORY1, F_OK));
    assert(0 == access(FILE1, F_OK));
    assert(0 == access(FILE2, F_OK));
    assert(0 != access(FILE1_IN_NEW_LOCATION, F_OK));
}


// rename(), name list, impossible operation
void case04() {
    int ret;
    ret = rename(MP"Poetry/yeats.txt\\names", MP"new_name");
    assert(ret == -1 && errno == EPERM);
    ret = rename(MP"Poetry/", MP"Poetry/yeats.txt\\names");
    /*
    Execution does not reach the rename() operation;
    FUSE stops execution after calls to getattr() for
    two arguments of rename()
    */
    assert(ret == -1 && errno == EINVAL);
}


// rename(), file from name list
void case05() {
    const char* ORIG_FILE = MP"Poetry/yeats.txt";
    const char* NAME_LIST = MP"Poetry/yeats.txt\\names";
    const char* CATS      = DS".vitis/00/00/00/00/08/categories";
    const char* NAME_LIST_FILE1 = MP"Poetry/yeats.txt\\names/:c2:yeats.txt";
    const char* NEW_NAME = MP"Poetry/Cloths_of_Heaven";
    int ret;
    ret = rename(NAME_LIST_FILE1, NEW_NAME);
    assert(ret == 0);
    assert(0 == access(NEW_NAME, F_OK));
    assert(0 != access(ORIG_FILE, F_OK));

    check_dds_file(CATS, 2, "Cloths_of_Heaven", 1, 3);
    check_dds_file(CATS, 5, "yeats.txt",        2, 3);
    check_dds_file(CATS, 6, "yeats.txt",        3, 3);
}
