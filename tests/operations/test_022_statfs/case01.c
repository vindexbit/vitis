#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <unistd.h>

#include "../common.h"

#define MAX_NAME_LENGTH 241

int main() {
    struct statvfs vfs;
    int ret = statvfs(MP"", &vfs);
    if (ret == -1) {
        perror("statvfs");
        return 1;
    }
    assert(vfs.f_namemax == MAX_NAME_LENGTH);

    ret = statvfs(MP"category1", &vfs);
    if (ret == -1) {
        perror("statvfs");
        return 1;
    }
    assert(vfs.f_namemax == MAX_NAME_LENGTH);

    return 0;
}
