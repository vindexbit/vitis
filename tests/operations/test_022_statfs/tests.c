#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/statvfs.h>
#include <unistd.h>

#include "../../common.h"


void case01();


int main(int argc, char** argv) {
    assert(argc == 2);
    int case_id = atoi(argv[1]);
    assert(case_id > 0);

    char wrong_case_id_msg[32] = {0};
    sprintf(wrong_case_id_msg, "Wrong case ID: %d", case_id);

    switch(case_id) {
        case 1: case01(); break;
        default: fprintf(stderr, "%s\n", wrong_case_id_msg); assert(0);
    }
    return 0;
}


#define MAX_NAME_LENGTH 232


void case01() {
    struct statvfs vfs;
    int ret = statvfs(MP"", &vfs);
    if (ret == -1) {
        perror("statvfs");
        assert(0);
    }
    assert(vfs.f_namemax == MAX_NAME_LENGTH);

    ret = statvfs(MP"category1", &vfs);
    if (ret == -1) {
        perror("statvfs");
        assert(0);
    }
    assert(vfs.f_namemax == MAX_NAME_LENGTH);
}
