import os
import sys
sys.path.append('../')

import pytest

from conftest import fs_empty_environment, MP, DS, run_c_test
from common import get_caseno

CATEGORY1 = os.path.join(MP, 'category1')
SYMLINK1 = os.path.join(CATEGORY1, 'symlink1')
BROKEN_SYMLINK2 = os.path.join(MP, 'symlink2')


def run(case_id: int):
    run_c_test(case_id, os.path.dirname(__file__))


def test_case01(fs_empty_environment):
    """
    readlink(), one symbolic link in one category
    """
    os.mkdir(CATEGORY1)
    os.symlink('/usr/bin/gcc-next', SYMLINK1)
    run(get_caseno())


def test_case02(fs_empty_environment):
    """
    readlink(), one symbolic link to existent non-Vitis directory
    """
    os.mkdir(CATEGORY1)
    os.symlink('/usr/bin/', SYMLINK1)
    run(get_caseno())


def test_case03(fs_empty_environment):
    """
    readlink(), symbolic link to non-existent category
    """
    os.symlink('non-existent/', BROKEN_SYMLINK2)
    run(get_caseno())


def test_case04(fs_empty_environment):
    """
    readlink(), imaginary link
    """
    os.mkdir(CATEGORY1)
    run(get_caseno())
