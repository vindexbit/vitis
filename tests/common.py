#!/usr/bin/env python3

import inspect
import os
import stat
import subprocess
import time, datetime
import pathlib
import re
import shutil

import pytest


def get_output(cmd_args: list):
    """Returns exit code, stdout and stderr messages by some command."""
    process = subprocess.Popen(
        cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    (stdout_content, stderr_content) = process.communicate()
    stdout_content = stdout_content.decode()
    stderr_content = stderr_content.decode()
    exit_code = process.wait()
    return exit_code, stdout_content, stderr_content


def mkdirs(dirs: list):
    """Creates directories recursively if don't exist like "mkdir -p"."""
    for d in dirs:
        if os.path.exists(d) and os.path.isdir(d):
            continue
        os.makedirs(d)


def write_to_file(filepath: str, content: str):
    """Writes some string to some file."""
    with open(filepath, 'w') as f:
        f.write(content)


def write_bytes_to_file(filepath: str, content: bytes):
    """Writes some string to some file."""
    with open(filepath, 'wb') as f:
        f.write(content)


def touch(filepath: str, acc_time: str = '', mod_time: str = ''):
    """
    Creates file and assigns timestamps of last access and last modification
    by time format "YYYY-mm-dd HH:MM:SS".
    """
    if not os.path.exists(filepath):
        pathlib.Path(filepath).touch()
    fmt = '%Y-%m-%d %H:%M:%S'
    st: os.stat_result = os.stat(filepath)
    if acc_time != '':
        acc_ts = datetime.datetime.strptime(acc_time, fmt).timestamp()
    else:
        acc_ts = st.st_atime
    if mod_time != '':
        mod_ts = datetime.datetime.strptime(mod_time, fmt).timestamp()
    else:
        mod_ts = acc_ts = st.st_mtime
    os.utime(filepath, (acc_ts, mod_ts))


def detach_loop(device: str):
    """Detaches some loop device."""
    subprocess.Popen(['losetup', '-d', device]).wait()


def umount(path: str):
    """Unmounts device or directory."""
    subprocess.Popen(['umount', path]).wait()


def umount_fuse(mountpoint: str):
    cmd = ['fusermount', '-uz', mountpoint]
    subprocess.Popen(cmd).wait()


def get_output(cmd_args: list):
    """Returns exit code, stdout and stderr messages by some command."""
    process = subprocess.Popen(
        cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    (stdout_content, stderr_content) = process.communicate()
    stdout_content = stdout_content.decode()
    stderr_content = stderr_content.decode()
    exit_code = process.wait()
    return exit_code, stdout_content, stderr_content


def get_caseno() -> int:
    match_obj = re.search(r'\d+$', inspect.stack()[1].function)
    return 0 if not match_obj else int(match_obj.group())


MODE_111 = stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH  # execute
MODE_222 = stat.S_IWUSR | stat.S_IWGRP | stat.S_IWOTH  # write
MODE_444 = stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH  # read
MODE_555 = MODE_111 | MODE_444
MODE_666 = MODE_444 | MODE_222
MODE_777 = MODE_666 | MODE_111
