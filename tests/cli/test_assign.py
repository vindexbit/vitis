#!/usr/bin/env python3

import os, subprocess
import sys

import pytest

sys.path.append('..')
from conftest import make_cmd_with_conf, MP
from conftest import fs_environment, fs_extended_environment
from conftest import FILE1, FILE2, FILE3
import common


class TestAssignAlias:
    """
    vitis assign -c <category> -a <alias>
    """

    @staticmethod
    def test_case01(fs_environment):
        """
        Simple usage of "vitis assign -c <category> -a <alias>"
        """
        cmd = make_cmd_with_conf('assign -c Moon -a Luna')
        subprocess.Popen(cmd).wait()
        assert os.readlink(f'{MP}/Luna') == 'Moon/'

    @staticmethod
    def test_case02(fs_environment):
        """
        Wrong usage of "vitis assign -c <category> -a <alias>":
        alias exists
        """
        os.mkdir(os.path.join(MP, 'Luna'))
        cmd = make_cmd_with_conf('assign -c Moon -a Luna')
        ret, out, err = common.get_output(cmd)
        assert err == 'Luna: such file exists.\n'

    @staticmethod
    def test_case03(fs_environment):
        """
        Wrong usage of "vitis assign -c <category> -a <alias>":
        Required category name is not a category (but regular file)
        """
        cmd = make_cmd_with_conf('assign -c Poetry/yeats.txt -a Luna')
        ret, out, err = common.get_output(cmd)
        assert err == 'Poetry/yeats.txt: file exists and it is not category.\n'


class TestAssignCategories:
    """
    vitis assign -f <file> ... [-c <category> ...] [--yes]
    """

    @staticmethod
    def test_case01(fs_extended_environment):
        """
        One category, one file.
        """
        cmd = make_cmd_with_conf(f'assign -f {FILE1} -c MainCategory --yes')
        subprocess.Popen(cmd).wait()
        filename = os.path.basename(FILE1)
        expected_path = os.path.join(MP, 'MainCategory', filename)
        assert os.path.exists(expected_path)

    @staticmethod
    def test_case02(fs_extended_environment):
        """
        Two categories, one file.
        """
        cmd = make_cmd_with_conf(f'assign -f {FILE1} -c Cat1 Cat2 --yes')
        subprocess.Popen(cmd).wait()
        name = os.path.basename(FILE1)
        path1 = os.path.join(MP, 'Cat1', name)
        path2 = os.path.join(MP, 'Cat2', name)
        assert os.path.exists(path1)
        assert os.path.exists(path2)
        assert os.stat(path1).st_ino == os.stat(path2).st_ino

    @staticmethod
    def test_case03(fs_extended_environment):
        """
        One category, two files.
        """
        cmd = make_cmd_with_conf(f'assign -f {FILE1} {FILE2} -c Cat1 --yes')
        subprocess.Popen(cmd).wait()
        path1 = os.path.join(MP, 'Cat1', os.path.basename(FILE1))
        path2 = os.path.join(MP, 'Cat1', os.path.basename(FILE2))
        assert os.path.exists(path1)
        assert os.path.exists(path2)
        assert os.stat(path1).st_ino != os.stat(path2).st_ino
