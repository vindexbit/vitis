#!/usr/bin/env python3

import os
import subprocess
import sys
from pathlib import Path
import shutil
import time

import pytest

sys.path.append('..')
from common import detach_loop, get_output, umount, umount_fuse
from conftest import make_cmd, get_st_dev


IM = '/tmp/ds.fs'
MP = '/mnt/vitis'
DEF_MP = '/etc/vitis/loop81-vitis'
LOOP = '/dev/loop81'
PRIMARY_MP = '/etc/vitis/loop81'


@pytest.fixture()
def env_for_case01():
    # setup
    cmd = make_cmd(f'mkfs -i {IM}')
    subprocess.Popen(cmd).wait()
    os.mkdir(MP)

    # test break
    yield

    # teardown
    umount_fuse(MP)
    os.rmdir(MP)
    detach_loop(LOOP)
    umount(PRIMARY_MP)
    os.rmdir(PRIMARY_MP)
    os.remove(IM)
    os.remove(DEF_MP)  # remove symlink


def test_case01(env_for_case01):
    """Simple usage of 'vitis mount'."""
    cmd = make_cmd(f'mount -i {IM} -m {MP}')
    subprocess.Popen(cmd).wait()
    time.sleep(1)
    base_dev = get_st_dev(os.path.dirname(MP))
    vitis_dev = get_st_dev(MP)
    assert vitis_dev != -1
    assert base_dev != vitis_dev


@pytest.fixture()
def env_for_case02():
    # setup
    time.sleep(1)
    cmd = make_cmd(f'mkfs -i {IM}')
    subprocess.Popen(cmd).wait()

    # test break
    yield

    # teardown
    umount_fuse(DEF_MP)
    os.rmdir(DEF_MP)
    detach_loop(LOOP)
    umount(PRIMARY_MP)
    os.rmdir(PRIMARY_MP)
    os.remove(IM)


def test_case02(env_for_case02):
    """Mount point by default."""
    assert not Path(DEF_MP).exists()
    cmd = make_cmd(f'mount -i {IM}')
    subprocess.Popen(cmd).wait()
    time.sleep(1)
    assert Path(DEF_MP).exists()
    base_dev = get_st_dev(os.path.dirname(DEF_MP))
    vitis_dev = get_st_dev(DEF_MP)
    assert vitis_dev != -1
    assert base_dev != vitis_dev


@pytest.fixture()
def env_for_case03():
    # setup
    cmd = make_cmd(f'mkfs -i {IM}')
    subprocess.Popen(cmd).wait()
    os.mkdir(MP)

    # test break
    yield

    # teardown
    umount_fuse(MP)
    os.rmdir(MP)
    detach_loop(LOOP)
    umount(PRIMARY_MP)
    os.rmdir(PRIMARY_MP)
    os.remove(IM)
    os.remove(DEF_MP)  # remove symlink


def test_case03(env_for_case03):
    """Repeated mount."""
    cmd = make_cmd(f'mount -i {IM} -m {MP}')
    subprocess.Popen(cmd).wait()
    time.sleep(1)
    base_dev = get_st_dev(os.path.dirname(MP))
    vitis_dev = get_st_dev(MP)
    assert vitis_dev != -1
    assert base_dev != vitis_dev
    # repeat
    cmd = make_cmd(f'mount -i {IM} -m {MP}')
    exit_code, stdout, stderr = get_output(cmd)
    assert exit_code == 1  # code of common error
    expected_err = 'This file already associated with some loop device.'
    expected_err += '\nCheck: losetup -l\n'
    assert stderr == expected_err

